#include "logger.hpp"

namespace Log
{

const LogLevel LOG_LEVEL = ERROR;

void Show(LogLevel l, string msg)
{
    switch (l)
    {
    case INFO:
        if (LOG_LEVEL < DEBUG)
            print_colored(msg, WHITE, BG_DEFAULT);
        break;
    case DEBUG:
        if (LOG_LEVEL < WARNING)
            print_colored(msg, DARK_GREEN, BG_DEFAULT);
        break;
    case WARNING:
        if (LOG_LEVEL < ERROR)
            print_colored(msg, DARK_YELLOW, BG_DEFAULT);
        break;
    case ERROR:
        print_colored(msg, RED, BG_DEFAULT);
        break;
    }
}

void Indent(LogLevel l, uint indent_level, string indent_symbol = string(""))
{
    if (LOG_LEVEL <= l)
    {
        for (uint i = 0; i < indent_level; i++)
        {
            std::cout << "    ";
        }
        print_colored(indent_symbol, DARK_MAGENTA, BG_DEFAULT);
    }
}

void NewLine(LogLevel l)
{
    if (LOG_LEVEL <= l)
    {
        std::cout << std::endl;
    }
}

} // namespace Log
