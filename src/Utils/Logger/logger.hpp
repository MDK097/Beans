#pragma once
#include "../Rainbow/C++/rainbow.hpp"

#include <sstream>
#include <string>

using sstream = std::stringstream;
using std::string;

namespace Log
{

using std::string;

enum LogLevel
{
    INFO,
    DEBUG,
    WARNING,
    ERROR
};

void Show(LogLevel l, string msg);
void Indent(LogLevel l, uint indent_level, string indent_symbol);
void NewLine(LogLevel l);

extern const LogLevel LOG_LEVEL;

} // namespace Log