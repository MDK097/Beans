#pragma once

#include <filesystem>
#include <gdkmm/rgba.h>
#include <sstream>
#include <string>
#include <unistd.h>

namespace Utils
{

std::string get_user();
int         hex_str_to_int(std::string str);
Gdk::RGBA   hex_argb_to_rgba(std::string argb);
std::string int_to_hex_str(int i);
std::string rgba_to_hex_argb(Gdk::RGBA rgba);
std::string double_to_string(double value);

template <typename T>
std::string inline to_string(T arg)
{
    std::stringstream s;
    std::string       str;
    s << arg;
    s >> str;
    return str;
}

} // namespace Utils
