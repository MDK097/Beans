#include "utils.hpp"

#include <cmath>

namespace Utils
{

std::string get_user()
{
    std::string str(getlogin());
    return str;
}

int hex_str_to_int(std::string str)
{
    std::stringstream s;
    int               i;
    s << str;
    s >> std::hex >> i;
    return i;
}

std::string int_to_hex_str(int i)
{
    std::stringstream s;
    s << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << i;
    return s.str();
}

Gdk::RGBA hex_argb_to_rgba(std::string argb)
{
    Gdk::RGBA rgba;
    if (argb.length() == 4 or argb.length() == 7)
    {
        rgba.set(argb);
    }
    else if (argb.length() == 5)
    {
        std::string temp;
        bool        capture = false;
        std::string alpha;
        for (auto c : argb)
        {
            if (c == '#')
            {
                capture = true;
                temp.push_back(c);
                continue;
            }
            if (capture)
            {
                alpha.push_back(c);
                capture = false;
                continue;
            }
            temp.push_back(c);
        }
        rgba.set(temp);
        rgba.set_alpha((double)hex_str_to_int(alpha) / 255.0);
    }
    else if (argb.length() == 9)
    {
        std::string   temp;
        bool          capture = false;
        std::string   alpha;
        unsigned char counter = 0;
        for (auto c : argb)
        {
            if (c == '#')
            {
                capture = true;
                temp.push_back(c);
                continue;
            }
            if (capture)
            {
                alpha.push_back(c);
                counter++;
                if (counter == 2)
                    capture = false;
                continue;
            }
            temp.push_back(c);
        }
        rgba.set(temp);
        rgba.set_alpha((double)hex_str_to_int(alpha) / 255.0);
    }
    return rgba;
}

std::string rgba_to_hex_argb(Gdk::RGBA rgba)
{
    std::string hex("#");
    hex.append(int_to_hex_str(std::ceil((rgba.get_alpha() * 255))));
    hex.append(int_to_hex_str(std::ceil((rgba.get_red() * 255))));
    hex.append(int_to_hex_str(std::ceil((rgba.get_green() * 255))));
    hex.append(int_to_hex_str(std::ceil((rgba.get_blue() * 255))));
    return hex;
}

std::string double_to_string(double value)
{
    std::stringstream s;
    std::string result;
    s << value;
    s >> result;
    return result;
}
} // namespace Utils
