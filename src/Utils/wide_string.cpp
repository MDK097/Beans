#include "wide_string.hpp"

#include <cctype>
#include <iomanip>
#include <iostream>
#include <sstream>

namespace Utils
{

std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

WideString::WideString()
{
    this->w_buffer = converter.from_bytes("");
}

WideString::WideString(const std::string str)
{
    this->w_buffer = converter.from_bytes(str);
}

WideString::WideString(const Utils::WideString &w_str)
{
    this->w_buffer = w_str.w_buffer;
}

WideString::WideString(const char *str_literal)
{
    this->w_buffer = converter.from_bytes(str_literal);
}

WideString::WideString(const std::wstring w_str)
{
    this->w_buffer = w_str;
}

WideString &WideString::operator=(const std::string &str)
{
    this->w_buffer = converter.from_bytes(str);
    return *this;
}

Utils::WideString &WideString::operator=(const char *c_str)
{
    this->w_buffer = converter.from_bytes(c_str);
    return *this;
}

WideString &WideString::operator=(const WideString &w_str)
{
    this->w_buffer = w_str.w_buffer;
    return *this;
}

bool WideString::operator==(const WideString &w_str) const
{
    if (this->w_buffer == w_str.w_buffer)
        return true;
    else
        return false;
}

WideString &WideString::operator=(const wstring &w_str)
{
    this->w_buffer = w_str;
    return *this;
}

bool WideString::operator!=(const WideString &w_str) const
{
    if (this->w_buffer == w_str.w_buffer)
        return false;
    else
        return true;
}

std::string WideString::str() const
{
    std::string temp(converter.to_bytes(this->w_buffer));
    return temp;
}

std::wstring WideString::w_str() const
{
    return this->w_buffer;
}

const char *WideString::c_str() const
{
    return this->str().c_str();
}

std::ostream &operator<<(std::ostream &os, const WideString &w)
{
    os << converter.to_bytes(w.w_str());
    return os;
}

bool WideString::operator<(const WideString &w_str) const
{
    if (this->w_buffer < w_str.w_buffer)
        return true;
    else
        return false;
}

void WideString::get_line_from(std::istream &(*const func_ptr)(std::istream &, std::string &),
                               std::istream &f)
{
    std::string temp;
    func_ptr(f, temp);
    this->w_buffer = converter.from_bytes(temp);
}

bool WideString::operator==(const std::string &str) const
{
    if (this->w_buffer == converter.from_bytes(str))
        return true;
    else
        return false;
}

bool WideString::operator==(const char *str) const
{
    if (this->w_buffer == converter.from_bytes(str))
        return true;
    else
        return false;
}

double WideString::parse_double()
{
    double            result = 0, multiplier = 1;
    int               i;
    std::stringstream s;
    for (auto w : this->w_buffer)
    {
        if (w < 256)
        {
            if (isdigit((char)w) and multiplier >= 1)
            {
                s << (char)w;
                s >> i;
                result *= multiplier;
                result += i;
                multiplier = 10;
                s.clear();
            }
            else if (isdigit((char)w) and multiplier < 1)
            {
                s << (char)w;
                s >> i;
                double temp = i * multiplier;
                result += temp;
                multiplier /= 10;
                s.clear();
            }
            else if ((char)w == '.')
            {
                multiplier = 0.1;
            }
            else
            {
                break;
            }
        }
    }
    return result;
}

int WideString::parse_int()
{
    int               i, result = 0, multiplier = 1;
    std::stringstream s;
    for (auto w : this->w_buffer)
    {
        if (w < 256)
        {
            if (isdigit((char)w))
            {
                s << (char)w;
                s >> i;
                result *= multiplier;
                result += i;
                multiplier = 10;
                s.clear();
            }
            else
            {
                break;
            }
        }
    }
    return result;
}

WideString WideString::get_symbol_after_double()
{
    std::wstring w_str;

    for (auto w : this->w_buffer)
    {
        if (w < 256 and (isdigit((char)w) or (char) w == '.'))
        {
            continue;
        }
        w_str.push_back(w);
    }
    WideString result(w_str);
    return result;
}
} // namespace Utils
