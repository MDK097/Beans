#pragma once
#include <codecvt>
#include <locale>
#include <ostream>
#include <string>

using std::string;
using std::wstring;

static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

namespace Utils
{
class WideString
{
    private:
    wstring w_buffer;

    public:
    std::string  str() const;
    std::wstring w_str() const;
    const char * c_str() const;
    void         get_line_from(std::istream &(*const func_ptr)(std::istream &, std::string &),
                               std::istream &f);

    WideString &operator=(const string &str);
    WideString &operator=(const WideString &w_str);
    WideString &operator=(const wstring &w_str);
    WideString &operator=(const char *c_str);
    bool        operator==(const WideString &w_str) const;
    bool        operator==(const std::string &str) const;
    bool        operator==(const char *str) const;
    bool        operator!=(const WideString &w_str) const;
    bool        operator<(const WideString &w_str) const;

    inline std::size_t bytes() { return this->str().length(); }
    inline std::size_t length() { return this->w_buffer.length(); }
    inline auto        begin() { return this->w_buffer.begin(); }
    inline auto        end() { return this->w_buffer.end(); }
    inline auto        at(int i) { return this->w_buffer.at(i); }

    double     parse_double();
    int        parse_int();
    WideString get_symbol_after_double();

    WideString();
    WideString(const std::string str);
    WideString(const WideString &w_str);
    WideString(const char *str_literal);
    WideString(const std::wstring w_str);
};

std::ostream &operator<<(std::ostream &os, const WideString &w);

} // namespace Utils

namespace std
{
template <>
struct hash<Utils::WideString>
{
    std::size_t operator()(Utils::WideString const &w) const noexcept
    {
        return std::hash<std::wstring>{}(w.w_str());
    }
};
} // namespace std