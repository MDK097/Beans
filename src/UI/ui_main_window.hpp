#pragma once

#include "../Beans.hpp"
#include "ui.hpp"
#include "ui_polybar.hpp"

class UIPolybar;

namespace Beans
{
namespace UI
{

class UIMain : public UITemplate
{
    private:
    std::shared_ptr<Gtk::StackSidebar> stack_sidebar;

    /* Methods */
    void initialize_polybar_page();

    public:
    UIMain();
    std::shared_ptr<UIPolybar> ui_polybar;
};
} // namespace UI
} // namespace Beans
