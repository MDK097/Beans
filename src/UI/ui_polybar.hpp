#pragma once

#include "../Utils/wide_string.hpp"
#include "ui.hpp"
#include "ui_main_window.hpp"
#include "ui_polybar_helper.hpp"

#include <gtkmm-3.0/gtkmm.h>
#include <thread>

#define HashMap std::unordered_map

using Utils::WideString;

namespace Beans
{
class Manager;

namespace UI
{

class UIPolybar;

class UIColorConfig : public UITemplate
{
    friend class UIPolybar;

    private:
    /* Attributes */
    std::shared_ptr<Gtk::Button>      cancel_button;
    std::shared_ptr<Gtk::Button>      apply_button;
    std::shared_ptr<Gtk::Entry>       color_name_entry;
    std::shared_ptr<Gtk::SpinButton>  spin_button;
    std::shared_ptr<Gtk::ColorButton> color_button;

    WideString selected_color_name;
    bool       is_creating_new_color;

    /* Methods */
    void on_cancel_button_clicked();
    void on_apply_button_clicked();

    public:
    UIColorConfig(std::shared_ptr<Gtk::Button>      t_cancel_button,
                  std::shared_ptr<Gtk::Button>      t_apply_button,
                  std::shared_ptr<Gtk::ColorButton> t_color_button,
                  std::shared_ptr<Gtk::Entry>       t_color_name_entry,
                  std::shared_ptr<Gtk::SpinButton>  t_spin_button,
                  std::shared_ptr<Gtk::Window>      t_window);
};

class UIListSelection : public UITemplate
{
    friend class UIPolybar;

    private:
    std::shared_ptr<Gtk::TreeView> tree_view;
    std::shared_ptr<Gtk::Button>   up_button;
    std::shared_ptr<Gtk::Button>   down_button;
    std::shared_ptr<Gtk::Button>   cancel_button;
    std::shared_ptr<Gtk::Button>   apply_button;

    void on_cancel_button_clicked();
    void on_apply_button_clicked();

    public:
    UIListSelection(std::shared_ptr<Gtk::TreeView> t_tree_view,
                    std::shared_ptr<Gtk::Button>   t_up_button,
                    std::shared_ptr<Gtk::Button>   t_down_button,
                    std::shared_ptr<Gtk::Button>   t_cancel_button,
                    std::shared_ptr<Gtk::Button>   t_apply_button,
                    std::shared_ptr<Gtk::Window>   t_window);
};

class DialogDelete : public DialogTemplate
{
    friend class UIPolybar;

    private:
    /* Attributes */
    std::shared_ptr<Gtk::Button> no_button;
    std::shared_ptr<Gtk::Button> yes_button;
    std::shared_ptr<Gtk::Label>  label;

    /* Methods */
    void on_no_button_clicked();
    void on_yes_button_clicked();
    void on_dialog_response(int i);

    public:
    DialogDelete(std::shared_ptr<Gtk::Button> t_no_button,
                 std::shared_ptr<Gtk::Button> t_yes_button,
                 std::shared_ptr<Gtk::Label>  t_label,
                 std::shared_ptr<Gtk::Dialog> t_dialog);
};

class ColorModelColumns : public Gtk::TreeModel::ColumnRecord
{
    public:
    ColorModelColumns();
    Gtk::TreeModelColumn<Glib::ustring> name;
    Gtk::TreeModelColumn<Glib::ustring> argb;
};

class BarModelColumns : public Gtk::TreeModel::ColumnRecord
{
    public:
    BarModelColumns();
    Gtk::TreeModelColumn<Glib::ustring> name;
};

class UIPolybar
{
    friend class UIPolybarHelper;
    friend class UIMain;

    struct ColorPage
    {
        std::shared_ptr<Gtk::Revealer> notification_revealer;
        std::shared_ptr<Gtk::Spinner>  notification_spinner;
        std::shared_ptr<Gtk::Label>    notification_label;
        std::shared_ptr<Gtk::TreeView> tree_view;
        std::shared_ptr<Gtk::Button>   save_button;
        std::shared_ptr<Gtk::Button>   delete_button;
        std::shared_ptr<Gtk::Button>   edit_button;
        std::shared_ptr<Gtk::Button>   new_button;
        Glib::RefPtr<Gtk::ListStore>   list_store;
        ColorModelColumns              model_columns;
    };

    struct BarSelectionPage
    {
        std::shared_ptr<Gtk::Revealer> notification_revealer;
        std::shared_ptr<Gtk::Spinner>  notification_spinner;
        std::shared_ptr<Gtk::Label>    notification_label;
        std::shared_ptr<Gtk::TreeView> tree_view;
        std::shared_ptr<Gtk::Button>   save_button;
        std::shared_ptr<Gtk::Button>   delete_button;
        std::shared_ptr<Gtk::Button>   edit_button;
        std::shared_ptr<Gtk::Button>   new_button;
        Glib::RefPtr<Gtk::ListStore>   list_store;
        BarModelColumns                model_columns;
    };

    struct BarConfigPage
    {
        std::shared_ptr<Gtk::Label>        bar_name_label;
        std::shared_ptr<Gtk::Revealer>     notification_revealer;
        std::shared_ptr<Gtk::Spinner>      notification_spinner;
        std::shared_ptr<Gtk::Label>        notification_label;
        std::shared_ptr<Gtk::Button>       back_button;
        std::shared_ptr<Gtk::Button>       save_button;
        std::shared_ptr<Gtk::ComboBoxText> monitor_combo_box;
        std::shared_ptr<Gtk::ComboBoxText> monitor_fallback_combo_box;
        std::shared_ptr<Gtk::CheckButton>  monitor_strict_check_button;
        std::shared_ptr<Gtk::CheckButton>  monitor_exact_check_button;
        std::shared_ptr<Gtk::CheckButton>  override_redirect_check_button;
        std::shared_ptr<Gtk::CheckButton>  bottom_check_button;
        std::shared_ptr<Gtk::CheckButton>  fixed_center_check_button;
        std::shared_ptr<Gtk::SpinButton>   width_spin_button;
        std::shared_ptr<Gtk::ComboBoxText> width_combo_box;
        std::shared_ptr<Gtk::SpinButton>   height_spin_button;
        std::shared_ptr<Gtk::ComboBoxText> height_combo_box;
        std::shared_ptr<Gtk::SpinButton>   offset_x_spin_button;
        std::shared_ptr<Gtk::ComboBoxText> offset_x_combo_box;
        std::shared_ptr<Gtk::SpinButton>   offset_y_spin_button;
        std::shared_ptr<Gtk::ComboBoxText> offset_y_combo_box;
        std::shared_ptr<Gtk::ComboBoxText> background_combo_box;
        std::shared_ptr<Gtk::Button>       background_gradient_edit_button;
        std::shared_ptr<Gtk::ComboBoxText> foreground_combo_box;
        std::shared_ptr<Gtk::SpinButton>   radius_top_spin_button;
        std::shared_ptr<Gtk::SpinButton>   radius_bottom_spin_button;
        std::shared_ptr<Gtk::SpinButton>   overline_size_spin_button;
        std::shared_ptr<Gtk::SpinButton>   underline_size_spin_button;
    };

    private:
    /* Attributes */
    enum Pages
    {
        COLOR_CONFIG_PAGE,
        BAR_SELECTION_PAGE,
        BAR_CONFIG_PAGE,
        MODULE_SELECTION_PAGE,
        MODULE_CONFIG_PAGE
    };

    std::unique_ptr<ColorPage>        color_page;
    std::unique_ptr<BarSelectionPage> bar_selection_page;
    std::unique_ptr<BarConfigPage>    bar_config_page;
    std::unique_ptr<UIColorConfig>    color_config_window;
    std::unique_ptr<UIListSelection>  list_selection_window;
    std::unique_ptr<DialogDelete>     delete_dialog;

    std::shared_ptr<HashMap<string, WideString>> selected_bar;
    std::shared_ptr<Gtk::Stack>                  stack;
    std::shared_ptr<Gtk::Stack>                  bars_substack;

    std::shared_ptr<std::thread> thread_to_execute;

    /* Methods */
    static void wait_and_hide(uint duration, std::shared_ptr<Gtk::Revealer> revealer);

    void on_color_page_back_button_clicked();
    void on_color_page_edit_button_clicked();
    void on_color_page_delete_button_clicked();
    void on_color_page_new_button_clicked();
    void on_color_page_save_button_clicked();

    void on_bar_selection_page_delete_button_clicked();
    void on_bar_selection_page_edit_button_clicked();
	void on_bar_selection_save_button_clicked();

	void on_bar_config_back_button_clicked();
    void on_bar_config_width_combo_box_changed();
    void on_bar_config_height_combo_box_changed();
    void on_bar_config_offset_x_combo_box_changed();
    void on_bar_config_offset_y_combo_box_changed();
    void on_bar_config_background_gradient_edit_button_clicked();
    void on_bar_config_save_button_clicked();

    void on_numerics_combo_box_changed(std::shared_ptr<Gtk::SpinButton> &  spin_button,
                                       std::shared_ptr<Gtk::ComboBoxText> &combo_box);

    void on_widget_changed();

    bool has_unsaved_modifications;
    bool can_flag_modified;
    bool bar_config_executed;

    void revealer_show(std::shared_ptr<Gtk::Revealer> revealer,
                       std::shared_ptr<Gtk::Spinner>  spinner,
                       std::shared_ptr<Gtk::Label>    label,
                       std::shared_ptr<Gtk::Button>   save_button);

    void revealer_hide(std::shared_ptr<Gtk::Revealer> revealer,
                       std::shared_ptr<Gtk::Label>    label,
                       std::shared_ptr<Gtk::Button>   save_button);

    void color_page_init(std::shared_ptr<Gtk::Revealer> t_notification_revealer,
                         std::shared_ptr<Gtk::Spinner>  t_notification_spinner,
                         std::shared_ptr<Gtk::Label>    t_notification_label,
                         std::shared_ptr<Gtk::TreeView> t_tree_view,
                         std::shared_ptr<Gtk::Button>   t_save_button,
                         std::shared_ptr<Gtk::Button>   t_delete_button,
                         std::shared_ptr<Gtk::Button>   t_edit_button,
                         std::shared_ptr<Gtk::Button>   t_new_button,
                         Glib::RefPtr<Gtk::ListStore>   t_list_store);

    void bar_selection_page_init(std::shared_ptr<Gtk::Revealer> t_notification_revealer,
                                 std::shared_ptr<Gtk::Spinner>  t_notification_spinner,
                                 std::shared_ptr<Gtk::Label>    t_notification_label,
                                 std::shared_ptr<Gtk::TreeView> t_tree_view,
                                 std::shared_ptr<Gtk::Button>   t_save_button,
                                 std::shared_ptr<Gtk::Button>   t_delete_button,
                                 std::shared_ptr<Gtk::Button>   t_edit_button,
                                 std::shared_ptr<Gtk::Button>   t_new_button,
                                 Glib::RefPtr<Gtk::ListStore>   t_list_store);

    void bar_config_page_init(std::shared_ptr<Gtk::Label>        t_bar_name_label,
                              std::shared_ptr<Gtk::Revealer>     t_notification_revealer,
                              std::shared_ptr<Gtk::Spinner>      t_notification_spinner,
                              std::shared_ptr<Gtk::Label>        t_notification_label,
                              std::shared_ptr<Gtk::Button>       t_back_button,
                              std::shared_ptr<Gtk::Button>       t_save_button,
                              std::shared_ptr<Gtk::ComboBoxText> t_monitor_combo_box,
                              std::shared_ptr<Gtk::ComboBoxText> t_monitor_fallback_combo_box,
                              std::shared_ptr<Gtk::CheckButton>  t_monitor_strict_check_button,
                              std::shared_ptr<Gtk::CheckButton>  t_monitor_exact_check_button,
                              std::shared_ptr<Gtk::CheckButton>  t_override_redirect_check_button,
                              std::shared_ptr<Gtk::CheckButton>  t_bottom_check_button,
                              std::shared_ptr<Gtk::CheckButton>  t_fixed_center_check_button,
                              std::shared_ptr<Gtk::SpinButton>   t_width_spin_button,
                              std::shared_ptr<Gtk::ComboBoxText> t_width_combo_box,
                              std::shared_ptr<Gtk::SpinButton>   t_height_spin_button,
                              std::shared_ptr<Gtk::ComboBoxText> t_height_combo_box,
                              std::shared_ptr<Gtk::SpinButton>   t_offset_x_spin_button,
                              std::shared_ptr<Gtk::ComboBoxText> t_offset_x_combo_box,
                              std::shared_ptr<Gtk::SpinButton>   t_offset_y_spin_button,
                              std::shared_ptr<Gtk::ComboBoxText> t_offset_y_combo_box,
                              std::shared_ptr<Gtk::ComboBoxText> t_background_combo_box,
                              std::shared_ptr<Gtk::Button>       t_background_gradient_edit_button,
                              std::shared_ptr<Gtk::ComboBoxText> t_foreground_combo_box,
                              std::shared_ptr<Gtk::SpinButton>   t_radius_top_spin_button,
                              std::shared_ptr<Gtk::SpinButton>   t_radius_bottom_spin_button,
                              std::shared_ptr<Gtk::SpinButton>   t_overline_size_spin_button,
                              std::shared_ptr<Gtk::SpinButton>   t_underline_size_spin_button);

    void bar_config_page_execute();

    void color_config_window_init();
    void list_selection_window_init();
    void delete_dialog_init();

    void inline join_thread()
    {
        if (this->thread_to_execute != nullptr)
        {
            this->thread_to_execute->join();
            this->thread_to_execute.reset();
        }
    }

    void inline flag_modified()
    {
        this->can_flag_modified = true;
        this->join_thread();
    }

    bool key_exists_in_selected_bar(const std::string &key);

    public:
    void color_page_list();
    void bar_selection_page_list();
    UIPolybar();
    ~UIPolybar();
};

} // namespace UI
} // namespace Beans
