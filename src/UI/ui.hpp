#pragma once

#include "../Utils/Logger/logger.hpp"

#include <filesystem>
#include <gtkmm-3.0/gtkmm.h>
#include <memory>
#include <string>

using std::string;

namespace Beans
{

namespace UI
{

class UITemplate
{
    protected:
    /* Attributes */
    Glib::RefPtr<Gtk::Builder>   builder;
    std::shared_ptr<Gtk::Window> window;
    std::filesystem::path        xml_file_path;

    /* Methods */
    template <typename T>
    std::shared_ptr<T> get_widget_from_builder(string widget_name)
    {
        T *pWidget;
        this->builder->get_widget(widget_name, pWidget);
        std::shared_ptr<T> sWidget(pWidget);
        return sWidget;
    }

    public:
    virtual void show();
    virtual ~UITemplate(){};
    inline Gtk::Window &get_window() { return *(this->window.get()); }
};

class DialogTemplate
{
    protected:
    std::shared_ptr<Gtk::Dialog> dialog;

    public:
    virtual int show();
    virtual ~DialogTemplate(){};
    inline Gtk::Dialog &get_dialog() { return *(this->dialog.get()); }
};

} // namespace UI
} // namespace Beans
