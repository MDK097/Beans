#include "ui_polybar.hpp"

#include "../Beans.hpp"

#include <chrono>
#include <iostream>
#include <thread>

namespace Beans
{
namespace UI
{

UIPolybar::UIPolybar()
{
    this->delete_dialog      = nullptr;
    this->color_page         = nullptr;
    this->bar_selection_page = nullptr;
    this->bar_config_page    = nullptr;
    this->selected_bar       = nullptr;

    this->has_unsaved_modifications = false;
    this->can_flag_modified         = false;
    this->bar_config_executed       = false;

    Beans::Manager::run_polybar_parser();
}

UIPolybar::~UIPolybar()
{
    if (this->thread_to_execute != nullptr)
    {
        this->thread_to_execute->join();
    }
}

void UIPolybar::color_page_init(std::shared_ptr<Gtk::Revealer> t_notification_revealer,
                                std::shared_ptr<Gtk::Spinner>  t_notification_spinner,
                                std::shared_ptr<Gtk::Label>    t_notification_label,
                                std::shared_ptr<Gtk::TreeView> t_tree_view,
                                std::shared_ptr<Gtk::Button>   t_save_button,
                                std::shared_ptr<Gtk::Button>   t_delete_button,
                                std::shared_ptr<Gtk::Button>   t_edit_button,
                                std::shared_ptr<Gtk::Button>   t_new_button,
                                Glib::RefPtr<Gtk::ListStore>   t_list_store)
{
    this->color_page = std::make_unique<ColorPage>();

    this->color_page->notification_revealer = t_notification_revealer;
    this->color_page->notification_spinner  = t_notification_spinner;
    this->color_page->notification_label    = t_notification_label;
    this->color_page->tree_view             = t_tree_view;
    this->color_page->save_button           = t_save_button;
    this->color_page->delete_button         = t_delete_button;
    this->color_page->edit_button           = t_edit_button;
    this->color_page->new_button            = t_new_button;
    this->color_page->list_store            = t_list_store;

    this->color_page->save_button->set_image_from_icon_name("document-save");
    this->color_page->edit_button->set_image_from_icon_name("edit");
    this->color_page->delete_button->set_image_from_icon_name("edit-delete");
    this->color_page->new_button->set_image_from_icon_name("document-new");

    this->color_page->notification_spinner->hide();
    this->color_page->notification_revealer->set_reveal_child(false);

    this->color_page->edit_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_color_page_edit_button_clicked));

    this->color_page->delete_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_color_page_delete_button_clicked));

    this->color_page->new_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_color_page_new_button_clicked));

    this->color_page->save_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_color_page_save_button_clicked));

    this->color_config_window->color_name_entry->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::flag_modified));

    this->color_config_window->color_button->signal_color_set().connect(
        sigc::mem_fun(*this, &UIPolybar::flag_modified));

    this->color_config_window->spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::flag_modified));
}

void UIPolybar::on_color_page_save_button_clicked()
{
    try
    {
        this->bar_config_page->notification_spinner->show();

        if (this->bar_config_executed)
        {
            UIPolybarHelper::save_bar_config(this);
        }

        Manager::save();

        this->has_unsaved_modifications = false;
        this->can_flag_modified         = false;
        this->bar_config_page->notification_spinner->hide();
        this->revealer_hide(this->color_page->notification_revealer,
                            this->color_page->notification_label,
                            this->color_page->save_button);

        this->bar_config_page->notification_revealer->set_reveal_child(false);
        this->bar_selection_page->notification_revealer->set_reveal_child(false);
    }
    catch (std::exception &e)
    {
        Log::Show(Log::LogLevel::ERROR, e.what());
    }
}

void UIPolybar::on_color_page_edit_button_clicked()
{
    this->color_config_window->is_creating_new_color = false;
    auto selected_row = this->color_page->tree_view->get_selection()->get_selected();

    if (not selected_row)
        return;

    auto      name_value = selected_row->get_value(this->color_page->model_columns.name);
    auto      argb       = selected_row->get_value(this->color_page->model_columns.argb);
    Gdk::RGBA rgba       = Utils::hex_argb_to_rgba(argb);
    double    alpha      = rgba.get_alpha();

    rgba.set_alpha(1.0);
    this->color_config_window->spin_button->set_value(alpha);
    this->color_config_window->color_name_entry->set_text(name_value);
    this->color_config_window->color_button->set_rgba(rgba);
    this->color_config_window->selected_color_name = name_value;
    this->color_config_window->show();
}

void UIPolybar::on_color_page_delete_button_clicked()
{
    auto selected_row = this->color_page->tree_view->get_selection()->get_selected();

    if (not selected_row)
        return;

    auto name_value = selected_row->get_value(this->color_page->model_columns.name);

    this->delete_dialog->label->set_text(
        Glib::ustring("Are you sure you want to delete this color: \"") + name_value + "\"?");

    int result = this->delete_dialog->show();

    switch (result)
    {
    case GTK_RESPONSE_NO:
    {
        return;
    }
    case GTK_RESPONSE_YES:
    {
        WideString w(name_value.c_str());
        Manager::remove_polybar_color_value(w);
    }
    }
    Manager::refresh_ui_polybar_color_list();
}

void UIPolybar::on_color_page_new_button_clicked()
{
    this->color_config_window->is_creating_new_color = true;
    Gdk::RGBA rgba;
    rgba.set_rgba(1.0, 1.0, 1.0);
    this->color_config_window->color_name_entry->set_text("New Color");
    this->color_config_window->color_name_entry->grab_focus();
    this->color_config_window->spin_button->set_value(rgba.get_alpha());
    this->color_config_window->color_button->set_rgba(rgba);
    this->color_config_window->show();
}

void UIPolybar::color_page_list()
{
    this->color_page->list_store->clear();
    for (auto entry : Manager::get_polybar_colors())
    {
        auto       row = this->color_page->list_store->append();
        WideString w(entry.first);
        row->set_value<Glib::ustring>(this->color_page->model_columns.name, w.str());
        row->set_value<Glib::ustring>(this->color_page->model_columns.argb, entry.second);
    }

    this->on_widget_changed();
}

ColorModelColumns::ColorModelColumns()
{
    this->add(this->name);
    this->add(this->argb);
}

UIColorConfig::UIColorConfig(std::shared_ptr<Gtk::Button>      t_cancel_button,
                             std::shared_ptr<Gtk::Button>      t_apply_button,
                             std::shared_ptr<Gtk::ColorButton> t_color_button,
                             std::shared_ptr<Gtk::Entry>       t_color_name_entry,
                             std::shared_ptr<Gtk::SpinButton>  t_spin_button,
                             std::shared_ptr<Gtk::Window>      t_window)
{
    this->window           = t_window;
    this->cancel_button    = t_cancel_button;
    this->apply_button     = t_apply_button;
    this->color_button     = t_color_button;
    this->spin_button      = t_spin_button;
    this->color_name_entry = t_color_name_entry;

    this->cancel_button->set_image_from_icon_name("window-close");
    this->apply_button->set_image_from_icon_name("dialog-ok");

    this->spin_button->set_range(0.0, 1.0);
    this->spin_button->set_digits(2);
    this->spin_button->set_increments(0.01, 0.01);

    this->cancel_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIColorConfig::on_cancel_button_clicked));
    this->apply_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIColorConfig::on_apply_button_clicked));
}

void UIColorConfig::on_cancel_button_clicked()
{
    this->window->close();
}

void UIColorConfig::on_apply_button_clicked()
{
    if (this->color_name_entry->get_text().length() == 0)
        return;

    Gdk::RGBA rgba;
    rgba = this->color_button->get_rgba();
    rgba.set_alpha(this->spin_button->get_value());
    string hex = Utils::rgba_to_hex_argb(rgba);

    if (this->is_creating_new_color)
    {
        auto       colors = Manager::get_polybar_colors();
        WideString w(this->color_name_entry->get_text().c_str());
        if (colors.find(w.w_str()) != colors.end())
        {
            return;
        }
    }
    else
    {
        WideString w(this->color_name_entry->get_text().c_str());
        if (this->selected_color_name != w)
            Manager::remove_polybar_color_value(this->selected_color_name);
    }

    WideString w(this->color_name_entry->get_text().c_str());
    Manager::set_polybar_color_value(w, hex);
    this->window->close();
    Manager::refresh_ui_polybar_color_list();
}

DialogDelete::DialogDelete(std::shared_ptr<Gtk::Button> t_no_button,
                           std::shared_ptr<Gtk::Button> t_yes_button,
                           std::shared_ptr<Gtk::Label>  t_label,
                           std::shared_ptr<Gtk::Dialog> t_dialog)
{
    this->no_button  = t_no_button;
    this->yes_button = t_yes_button;
    this->label      = t_label;
    this->dialog     = t_dialog;

    this->no_button->set_image_from_icon_name("dialog-cancel");
    this->yes_button->set_image_from_icon_name("dialog-ok");

    this->no_button->signal_clicked().connect(
        sigc::mem_fun(*this, &DialogDelete::on_no_button_clicked));
    this->yes_button->signal_clicked().connect(
        sigc::mem_fun(*this, &DialogDelete::on_yes_button_clicked));
    this->dialog->signal_response().connect(
        sigc::mem_fun(*this, &DialogDelete::on_dialog_response));
}

void DialogDelete::on_no_button_clicked()
{
    this->dialog->response(GTK_RESPONSE_NO);
}

void DialogDelete::on_yes_button_clicked()
{
    this->dialog->response(GTK_RESPONSE_YES);
}

void DialogDelete::on_dialog_response(int i)
{
    this->dialog->hide();
}

BarModelColumns::BarModelColumns()
{
    this->add(this->name);
}

void UIPolybar::bar_selection_page_init(std::shared_ptr<Gtk::Revealer> t_notification_revealer,
                                        std::shared_ptr<Gtk::Spinner>  t_notification_spinner,
                                        std::shared_ptr<Gtk::Label>    t_notification_label,
                                        std::shared_ptr<Gtk::TreeView> t_tree_view,
                                        std::shared_ptr<Gtk::Button>   t_save_button,
                                        std::shared_ptr<Gtk::Button>   t_delete_button,
                                        std::shared_ptr<Gtk::Button>   t_edit_button,
                                        std::shared_ptr<Gtk::Button>   t_new_button,
                                        Glib::RefPtr<Gtk::ListStore>   t_list_store)
{
    this->bar_selection_page = std::make_unique<BarSelectionPage>();

    this->bar_selection_page->notification_revealer = t_notification_revealer;
    this->bar_selection_page->notification_spinner  = t_notification_spinner;
    this->bar_selection_page->notification_label    = t_notification_label;
    this->bar_selection_page->tree_view             = t_tree_view;
    this->bar_selection_page->save_button           = t_save_button;
    this->bar_selection_page->delete_button         = t_delete_button;
    this->bar_selection_page->edit_button           = t_edit_button;
    this->bar_selection_page->new_button            = t_new_button;
    this->bar_selection_page->list_store            = t_list_store;

    this->bar_selection_page->notification_revealer->set_reveal_child(false);

    this->bar_selection_page->save_button->set_image_from_icon_name("document-save");
    this->bar_selection_page->edit_button->set_image_from_icon_name("edit");
    this->bar_selection_page->delete_button->set_image_from_icon_name("edit-delete");
    this->bar_selection_page->new_button->set_image_from_icon_name("document-new");

    this->bar_selection_page->save_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_selection_save_button_clicked));

    this->bar_selection_page->delete_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_selection_page_delete_button_clicked));

    this->bar_selection_page->edit_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_selection_page_edit_button_clicked));
}

void UIPolybar::on_bar_selection_save_button_clicked()
{
    try
    {
        this->bar_config_page->notification_spinner->show();

        if (this->bar_config_executed)
        {
            UIPolybarHelper::save_bar_config(this);
        }

        Manager::save();
        this->has_unsaved_modifications = false;
        this->can_flag_modified         = false;
        this->bar_config_page->notification_spinner->hide();
        this->revealer_hide(this->bar_selection_page->notification_revealer,
                            this->bar_selection_page->notification_label,
                            this->bar_selection_page->save_button);

        this->bar_config_page->notification_revealer->set_reveal_child(false);
        this->color_page->notification_revealer->set_reveal_child(false);
    }
    catch (std::exception &e)
    {
        Log::Show(Log::LogLevel::ERROR, e.what());
    }
}

void UIPolybar::bar_selection_page_list()
{
    this->bar_selection_page->list_store->clear();
    for (auto bar : Manager::get_polybar_bars())
    {
        auto       row = this->bar_selection_page->list_store->append();
        WideString w(bar->at("name"));
        row->set_value<Glib::ustring>(this->bar_selection_page->model_columns.name, w.str());
    }
}

void UIPolybar::on_bar_selection_page_delete_button_clicked()
{
    auto selected_row = this->bar_selection_page->tree_view->get_selection()->get_selected();

    if (not selected_row)
        return;

    auto name_value = selected_row->get_value(this->bar_selection_page->model_columns.name);

    this->delete_dialog->label->set_text(
        Glib::ustring("Are you sure you want to delete this bar: \"") + name_value + "\"?");

    int result = this->delete_dialog->show();

    switch (result)
    {
    case GTK_RESPONSE_NO:
    {
        return;
    }
    case GTK_RESPONSE_YES:
    {
        WideString w(name_value.c_str());
        Manager::remove_polybar_bar(w);
    }
    }
    Manager::refresh_ui_polybar_bar_list();
}

void UIPolybar::on_bar_selection_page_edit_button_clicked()
{
    auto selected_row = this->bar_selection_page->tree_view->get_selection()->get_selected();

    if (not selected_row)
        return;

    auto name_value = selected_row->get_value(this->bar_selection_page->model_columns.name);
    auto bar_list   = Manager::get_polybar_bars();
    for (auto bar : bar_list)
    {
        WideString w(bar->at("name"));
        if (w.str() == name_value)
        {
            this->selected_bar = bar;
            break;
        }
    }

    this->bar_config_page_execute();
    this->bars_substack->set_visible_child("polybar_bar_config_menu");
}

void UIPolybar::bar_config_page_init(
    std::shared_ptr<Gtk::Label>        t_bar_name_label,
    std::shared_ptr<Gtk::Revealer>     t_notification_revealer,
    std::shared_ptr<Gtk::Spinner>      t_notification_spinner,
    std::shared_ptr<Gtk::Label>        t_notification_label,
    std::shared_ptr<Gtk::Button>       t_back_button,
    std::shared_ptr<Gtk::Button>       t_save_button,
    std::shared_ptr<Gtk::ComboBoxText> t_monitor_combo_box,
    std::shared_ptr<Gtk::ComboBoxText> t_monitor_fallback_combo_box,
    std::shared_ptr<Gtk::CheckButton>  t_monitor_strict_check_button,
    std::shared_ptr<Gtk::CheckButton>  t_monitor_exact_check_button,
    std::shared_ptr<Gtk::CheckButton>  t_override_redirect_check_button,
    std::shared_ptr<Gtk::CheckButton>  t_bottom_check_button,
    std::shared_ptr<Gtk::CheckButton>  t_fixed_center_check_button,
    std::shared_ptr<Gtk::SpinButton>   t_width_spin_button,
    std::shared_ptr<Gtk::ComboBoxText> t_width_combo_box,
    std::shared_ptr<Gtk::SpinButton>   t_height_spin_button,
    std::shared_ptr<Gtk::ComboBoxText> t_height_combo_box,
    std::shared_ptr<Gtk::SpinButton>   t_offset_x_spin_button,
    std::shared_ptr<Gtk::ComboBoxText> t_offset_x_combo_box,
    std::shared_ptr<Gtk::SpinButton>   t_offset_y_spin_button,
    std::shared_ptr<Gtk::ComboBoxText> t_offset_y_combo_box,
    std::shared_ptr<Gtk::ComboBoxText> t_background_combo_box,
    std::shared_ptr<Gtk::Button>       t_background_gradient_edit_button,
    std::shared_ptr<Gtk::ComboBoxText> t_foreground_combo_box,
    std::shared_ptr<Gtk::SpinButton>   t_radius_top_spin_button,
    std::shared_ptr<Gtk::SpinButton>   t_radius_bottom_spin_button,
    std::shared_ptr<Gtk::SpinButton>   t_overline_size_spin_button,
    std::shared_ptr<Gtk::SpinButton>   t_underline_size_spin_button)
{
    this->bar_config_page = std::make_unique<BarConfigPage>();

    this->bar_config_page->bar_name_label                  = t_bar_name_label;
    this->bar_config_page->notification_revealer           = t_notification_revealer;
    this->bar_config_page->notification_spinner            = t_notification_spinner;
    this->bar_config_page->notification_label              = t_notification_label;
    this->bar_config_page->back_button                     = t_back_button;
    this->bar_config_page->save_button                     = t_save_button;
    this->bar_config_page->monitor_combo_box               = t_monitor_combo_box;
    this->bar_config_page->monitor_fallback_combo_box      = t_monitor_fallback_combo_box;
    this->bar_config_page->monitor_strict_check_button     = t_monitor_strict_check_button;
    this->bar_config_page->monitor_exact_check_button      = t_monitor_exact_check_button;
    this->bar_config_page->override_redirect_check_button  = t_override_redirect_check_button;
    this->bar_config_page->bottom_check_button             = t_bottom_check_button;
    this->bar_config_page->fixed_center_check_button       = t_fixed_center_check_button;
    this->bar_config_page->width_spin_button               = t_width_spin_button;
    this->bar_config_page->width_combo_box                 = t_width_combo_box;
    this->bar_config_page->height_spin_button              = t_height_spin_button;
    this->bar_config_page->height_combo_box                = t_height_combo_box;
    this->bar_config_page->offset_x_spin_button            = t_offset_x_spin_button;
    this->bar_config_page->offset_x_combo_box              = t_offset_x_combo_box;
    this->bar_config_page->offset_y_spin_button            = t_offset_y_spin_button;
    this->bar_config_page->offset_y_combo_box              = t_offset_y_combo_box;
    this->bar_config_page->background_combo_box            = t_background_combo_box;
    this->bar_config_page->background_gradient_edit_button = t_background_gradient_edit_button;
    this->bar_config_page->foreground_combo_box            = t_foreground_combo_box;
    this->bar_config_page->radius_top_spin_button          = t_radius_top_spin_button;
    this->bar_config_page->radius_bottom_spin_button       = t_radius_bottom_spin_button;
    this->bar_config_page->overline_size_spin_button       = t_overline_size_spin_button;
    this->bar_config_page->underline_size_spin_button      = t_underline_size_spin_button;

    this->bar_config_page->notification_revealer->set_reveal_child(false);

    this->bar_config_page->back_button->set_image_from_icon_name("go-previous");
    this->bar_config_page->save_button->set_image_from_icon_name("document-save");

    this->bar_config_page->back_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_config_back_button_clicked));

    this->bar_config_page->save_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_config_save_button_clicked));

    this->bar_config_page->background_gradient_edit_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_config_background_gradient_edit_button_clicked));

    this->bar_config_page->monitor_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->monitor_fallback_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->monitor_strict_check_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->monitor_exact_check_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->override_redirect_check_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->bottom_check_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->fixed_center_check_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->width_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->width_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_config_width_combo_box_changed));

    this->bar_config_page->height_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->height_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_config_height_combo_box_changed));

    this->bar_config_page->offset_x_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->offset_x_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_config_offset_x_combo_box_changed));

    this->bar_config_page->offset_y_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->offset_y_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_bar_config_offset_y_combo_box_changed));

    this->bar_config_page->background_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->foreground_combo_box->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->radius_top_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->radius_bottom_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->overline_size_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));

    this->bar_config_page->underline_size_spin_button->signal_changed().connect(
        sigc::mem_fun(*this, &UIPolybar::on_widget_changed));
}

void UIPolybar::on_bar_config_save_button_clicked()
{
    try
    {
        this->bar_config_page->notification_spinner->show();
        UIPolybarHelper::save_bar_config(this);
        Manager::save();
        this->has_unsaved_modifications = false;
        this->bar_config_page->notification_spinner->hide();
        this->revealer_hide(this->bar_config_page->notification_revealer,
                            this->bar_config_page->notification_label,
                            this->bar_config_page->save_button);

        this->bar_selection_page->notification_revealer->set_reveal_child(false);
        this->color_page->notification_revealer->set_reveal_child(false);
    }
    catch (std::exception &e)
    {
        Log::Show(Log::LogLevel::ERROR, e.what());
    }
}

void UIPolybar::bar_config_page_execute()
{
    this->can_flag_modified = false;
    this->bar_config_page->bar_name_label->set_text(this->selected_bar->at("name").str());
    UIPolybarHelper::assert_bar_config(this);
    this->can_flag_modified   = true;
    this->bar_config_executed = true;
}

void UIPolybar::on_bar_config_back_button_clicked()
{
    if (this->has_unsaved_modifications)
        UIPolybarHelper::save_bar_config(this);
    this->can_flag_modified = false;
    this->bars_substack->set_visible_child("polybar_bar_selection_menu");
    this->selected_bar.reset();
}

void UIPolybar::on_bar_config_width_combo_box_changed()
{
    this->on_numerics_combo_box_changed(this->bar_config_page->width_spin_button,
                                        this->bar_config_page->width_combo_box);
}

void UIPolybar::on_bar_config_height_combo_box_changed()
{
    this->on_numerics_combo_box_changed(this->bar_config_page->height_spin_button,
                                        this->bar_config_page->height_combo_box);
}

void UIPolybar::on_bar_config_offset_x_combo_box_changed()
{
    this->on_numerics_combo_box_changed(this->bar_config_page->offset_x_spin_button,
                                        this->bar_config_page->offset_x_combo_box);
}

void UIPolybar::on_bar_config_offset_y_combo_box_changed()
{
    this->on_numerics_combo_box_changed(this->bar_config_page->offset_y_spin_button,
                                        this->bar_config_page->offset_y_combo_box);
}

bool UIPolybar::key_exists_in_selected_bar(const std::string &key)
{
    if (this->selected_bar->find(key) == selected_bar->end())
        return false;
    else
        return true;
}

void UIPolybar::on_numerics_combo_box_changed(std::shared_ptr<Gtk::SpinButton> &  spin_button,
                                              std::shared_ptr<Gtk::ComboBoxText> &combo_box)
{
    if (combo_box->get_active_text() == "%")
    {
        spin_button->set_range(0, 100);
    }
    else
    {
        spin_button->set_range(0, 4096);
    }

    on_widget_changed();
}

void UIPolybar::on_bar_config_background_gradient_edit_button_clicked()
{
    this->list_selection_window->show();
    this->list_selection_window->cancel_button->set_image_from_icon_name("window-close");
    this->list_selection_window->apply_button->set_image_from_icon_name("dialog-ok");
    this->list_selection_window->up_button->set_image_from_icon_name("go-up");
    this->list_selection_window->down_button->set_image_from_icon_name("go-down");
}

UIListSelection::UIListSelection(std::shared_ptr<Gtk::TreeView> t_tree_view,
                                 std::shared_ptr<Gtk::Button>   t_up_button,
                                 std::shared_ptr<Gtk::Button>   t_down_button,
                                 std::shared_ptr<Gtk::Button>   t_cancel_button,
                                 std::shared_ptr<Gtk::Button>   t_apply_button,
                                 std::shared_ptr<Gtk::Window>   t_window)
{
    this->window        = t_window;
    this->cancel_button = t_cancel_button;
    this->apply_button  = t_apply_button;
    this->tree_view     = t_tree_view;
    this->up_button     = t_up_button;
    this->down_button   = t_down_button;

    this->cancel_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIListSelection::on_cancel_button_clicked));

    this->apply_button->signal_clicked().connect(
        sigc::mem_fun(*this, &UIListSelection::on_apply_button_clicked));
}

void UIListSelection::on_apply_button_clicked()
{
}

void UIListSelection::on_cancel_button_clicked()
{
    this->window->close();
}

void UIPolybar::wait_and_hide(uint duration, std::shared_ptr<Gtk::Revealer> revealer)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(duration * 1000));
    revealer->set_reveal_child(false);
}

void UIPolybar::on_widget_changed()
{
    if (this->can_flag_modified)
    {
        this->has_unsaved_modifications = true;
    }

    if (this->has_unsaved_modifications)
    {
        this->join_thread();

        this->revealer_show(this->bar_config_page->notification_revealer,
                            this->bar_config_page->notification_spinner,
                            this->bar_config_page->notification_label,
                            this->bar_config_page->save_button);

        this->revealer_show(this->bar_selection_page->notification_revealer,
                            this->bar_selection_page->notification_spinner,
                            this->bar_selection_page->notification_label,
                            this->bar_selection_page->save_button);

        this->revealer_show(this->color_page->notification_revealer,
                            this->color_page->notification_spinner,
                            this->color_page->notification_label,
                            this->color_page->save_button);
    }
}

void UIPolybar::revealer_show(std::shared_ptr<Gtk::Revealer> revealer,
                              std::shared_ptr<Gtk::Spinner>  spinner,
                              std::shared_ptr<Gtk::Label>    label,
                              std::shared_ptr<Gtk::Button>   save_button)
{
    spinner->hide();
    save_button->show();
    revealer->set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_UP);
    revealer->set_reveal_child(true);
    label->set_text("⚠ There are unsaved modifications");
}

void UIPolybar::revealer_hide(std::shared_ptr<Gtk::Revealer> revealer,
                              std::shared_ptr<Gtk::Label>    label,
                              std::shared_ptr<Gtk::Button>   save_button)
{
    label->set_text("✔ Modifications saved succesfully!");
    save_button->hide();
    revealer->set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_DOWN);
    this->thread_to_execute = std::make_shared<std::thread>(wait_and_hide, 2, revealer);
}

} // namespace UI
} // namespace Beans
