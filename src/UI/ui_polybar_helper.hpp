#pragma once
#include "ui_polybar.hpp"

namespace Beans
{
namespace UI
{

class UIPolybar;

class UIPolybarHelper
{
    private:
    static void check_int_values_with_combo_box(Beans::UI::UIPolybar *              ui_polybar,
                                                std::shared_ptr<Gtk::SpinButton> &  spin_button,
                                                std::shared_ptr<Gtk::ComboBoxText> &combo_box,
                                                std::string                         key);

    static void set_default_int_value_with_combo_box(Beans::UI::UIPolybar *            ui_polybar,
                                                     std::shared_ptr<Gtk::SpinButton> &spin_button,
                                                     std::shared_ptr<Gtk::ComboBoxText> &combo_box);

    static void check_float_value(Beans::UI::UIPolybar *            ui_polybar,
                                  std::shared_ptr<Gtk::SpinButton> &spin_button,
                                  double                            range_min,
                                  double                            range_max,
                                  double                            increment,
                                  std::string                       key);

    static void set_default_float_value(Beans::UI::UIPolybar *            ui_polybar,
                                        std::shared_ptr<Gtk::SpinButton> &spin_button,
                                        double                            range_min,
                                        double                            range_max,
                                        double                            increment);

    static bool        check_boolean(Beans::UI::UIPolybar *ui_polybar, std::string key);
    static double      check_double(Beans::UI::UIPolybar *ui_polybar, std::string key);
    static int         check_int(Beans::UI::UIPolybar *ui_polybar, std::string key);
    static std::string check_polybar_variable(Utils::WideString w_str);
    static std::string get_color_from_polybar_variable(std::string str);

    static void get_monitor_strict(Beans::UI::UIPolybar *ui_polybar);
    static void get_monitor_exact(Beans::UI::UIPolybar *ui_polybar);
    static void get_override_redirect(Beans::UI::UIPolybar *ui_polybar);
    static void get_bottom(Beans::UI::UIPolybar *ui_polybar);
    static void get_fixed_center(Beans::UI::UIPolybar *ui_polybar);
    static void get_width(Beans::UI::UIPolybar *ui_polybar);
    static void get_height(Beans::UI::UIPolybar *ui_polybar);
    static void get_offset(Beans::UI::UIPolybar *ui_polybar);
    static void get_colors(std::shared_ptr<Gtk::ComboBoxText> &combo_box);
    static void get_background(Beans::UI::UIPolybar *ui_polybar);
    static void get_background_gradient(Beans::UI::UIPolybar *ui_polybar);
    static void get_foreground(Beans::UI::UIPolybar *ui_polybar);
    static void get_radius(Beans::UI::UIPolybar *ui_polybar);
    static void get_line_size(Beans::UI::UIPolybar *ui_polybar);
    static void get_monitor(Beans::UI::UIPolybar *ui_polybar);
    static void get_monitor_fallback(Beans::UI::UIPolybar *ui_polybar);

    static void save_ui_combo_box(Beans::UI::UIPolybar *                    ui_polybar,
                                  HashMap<std::string, Utils::WideString> * p,
                                  std::string                               name,
                                  const std::shared_ptr<Gtk::ComboBoxText> &combo_box);

    static void save_ui_color_combo_box(Beans::UI::UIPolybar *                    ui_polybar,
                                        HashMap<std::string, Utils::WideString> * p,
                                        std::string                               name,
                                        const std::shared_ptr<Gtk::ComboBoxText> &combo_box);

    static void save_ui_spin_button(Beans::UI::UIPolybar *                   ui_polybar,
                                    HashMap<std::string, Utils::WideString> *p,
                                    std::string                              name,
                                    const std::shared_ptr<Gtk::SpinButton> & spin_button);

    static void
    save_ui_spin_button_with_combo_box(Beans::UI::UIPolybar *                    ui_polybar,
                                       HashMap<std::string, Utils::WideString> * p,
                                       std::string                               name,
                                       const std::shared_ptr<Gtk::ComboBoxText> &combo_box,
                                       const std::shared_ptr<Gtk::SpinButton> &  spin_button);

    static void save_ui_check_button(Beans::UI::UIPolybar *                   ui_polybar,
                                     HashMap<std::string, Utils::WideString> *p,
                                     std::string                              name,
                                     const std::shared_ptr<Gtk::CheckButton> &check_button);

    public:
    static void assert_bar_config(Beans::UI::UIPolybar *ui_polybar);
    static void save_bar_config(Beans::UI::UIPolybar *ui_polybar);
};

} // namespace UI
} // namespace Beans
