#include "ui_polybar_helper.hpp"

#include "../Beans.hpp"

#include <regex>

using std::regex;
using std::smatch;

const std::string NONE = "-- None --";

namespace Beans
{

namespace UI
{

bool UIPolybarHelper::check_boolean(Beans::UI::UIPolybar *ui_polybar, std::string key)
{
    if (ui_polybar->key_exists_in_selected_bar(key))
    {
        if (ui_polybar->selected_bar->at(key) == "true")
        {
            return true;
        }
        else if (ui_polybar->selected_bar->at(key) == "false")
        {
            return false;
        }
        else
        {
            Log::Show(Log::LogLevel::ERROR,
                      string("Syntax error on ") + key + " option in bar "
                          + ui_polybar->selected_bar->at("name").str() + "\n");
            return false;
        }
    }
    return false;
}

double UIPolybarHelper::check_double(Beans::UI::UIPolybar *ui_polybar, std::string key)
{
    if (ui_polybar->key_exists_in_selected_bar(key))
    {
        WideString value(ui_polybar->selected_bar->at(key));
        return value.parse_double();
    }
    return 0.f;
}

int UIPolybarHelper::check_int(Beans::UI::UIPolybar *ui_polybar, std::string key)
{
    if (ui_polybar->key_exists_in_selected_bar(key))
    {
        WideString value(ui_polybar->selected_bar->at(key));
        return value.parse_int();
    }
    return 0;
}

std::string UIPolybarHelper::check_polybar_variable(Utils::WideString w_str)
{
    smatch      matches;
    regex       var(".*\\$\\{(.*)\\}.*");
    std::string search_str(w_str.str());
    if (std::regex_search(search_str, matches, var))
    {
        return matches.str(1);
    }
    return std::string("");
}

std::string UIPolybarHelper::get_color_from_polybar_variable(std::string str)
{
    smatch matches;
    regex  color("colors\\.(.*)");
    if (std::regex_search(str, matches, color))
    {
        return matches.str(1);
    }
    return std::string("");
}

void UIPolybarHelper::get_monitor_strict(Beans::UI::UIPolybar *ui_polybar)
{
    ui_polybar->bar_config_page->monitor_strict_check_button->set_active(
        check_boolean(ui_polybar, "monitor-strict"));
}

void UIPolybarHelper::get_monitor_exact(Beans::UI::UIPolybar *ui_polybar)
{
    ui_polybar->bar_config_page->monitor_exact_check_button->set_active(
        check_boolean(ui_polybar, "monitor-exact"));
}

void UIPolybarHelper::get_override_redirect(Beans::UI::UIPolybar *ui_polybar)
{
    ui_polybar->bar_config_page->override_redirect_check_button->set_active(
        check_boolean(ui_polybar, "override-redirect"));
}

void UIPolybarHelper::get_bottom(Beans::UI::UIPolybar *ui_polybar)
{
    ui_polybar->bar_config_page->bottom_check_button->set_active(
        check_boolean(ui_polybar, "bottom"));
}

void UIPolybarHelper::get_fixed_center(Beans::UI::UIPolybar *ui_polybar)
{
    ui_polybar->bar_config_page->fixed_center_check_button->set_active(
        check_boolean(ui_polybar, "fixed-center"));
}

void UIPolybarHelper::check_int_values_with_combo_box(Beans::UI::UIPolybar *            ui_polybar,
                                                      std::shared_ptr<Gtk::SpinButton> &spin_button,
                                                      std::shared_ptr<Gtk::ComboBoxText> &combo_box,
                                                      std::string                         key)
{
    spin_button->set_increments(1.0, 5.0);
    spin_button->set_digits(0);
    if (ui_polybar->selected_bar->at(key).get_symbol_after_double() == "%")
    {
        spin_button->set_range(0, 100);
        combo_box->set_active_text("%");
    }
    else
    {
        spin_button->set_range(0, 4096);
        combo_box->set_active_text("px");
    }
    spin_button->set_value(ui_polybar->selected_bar->at(key).parse_double());
}

void UIPolybarHelper::set_default_int_value_with_combo_box(
    Beans::UI::UIPolybar *              ui_polybar,
    std::shared_ptr<Gtk::SpinButton> &  spin_button,
    std::shared_ptr<Gtk::ComboBoxText> &combo_box)
{
    spin_button->set_increments(1.0, 1.0);
    spin_button->set_digits(0);
    spin_button->set_range(0, 4096);
    combo_box->set_active_text("px");
    spin_button->set_value(0.0);
}

void UIPolybarHelper::check_float_value(Beans::UI::UIPolybar *            ui_polybar,
                                        std::shared_ptr<Gtk::SpinButton> &spin_button,
                                        double                            range_min,
                                        double                            range_max,
                                        double                            increment,
                                        std::string                       key)
{
    spin_button->set_increments(increment, 10 * increment);
    spin_button->set_digits(1);
    spin_button->set_range(range_min, range_max);
    spin_button->set_value(ui_polybar->selected_bar->at(key).parse_double());
}

void UIPolybarHelper::set_default_float_value(Beans::UI::UIPolybar *            ui_polybar,
                                              std::shared_ptr<Gtk::SpinButton> &spin_button,
                                              double                            range_min,
                                              double                            range_max,
                                              double                            increment)
{
    spin_button->set_increments(increment, 10 * increment);
    spin_button->set_digits(1);
    spin_button->set_range(range_min, range_max);
    spin_button->set_value(0.0);
}

void UIPolybarHelper::get_width(Beans::UI::UIPolybar *ui_polybar)
{
    if (not ui_polybar->key_exists_in_selected_bar("width"))
    {
        set_default_int_value_with_combo_box(ui_polybar,
                                             ui_polybar->bar_config_page->width_spin_button,
                                             ui_polybar->bar_config_page->width_combo_box);
        return;
    }

    check_int_values_with_combo_box(ui_polybar,
                                    ui_polybar->bar_config_page->width_spin_button,
                                    ui_polybar->bar_config_page->width_combo_box,
                                    "width");
}

void UIPolybarHelper::get_height(Beans::UI::UIPolybar *ui_polybar)
{
    if (not ui_polybar->key_exists_in_selected_bar("height"))
    {
        set_default_int_value_with_combo_box(ui_polybar,
                                             ui_polybar->bar_config_page->height_spin_button,
                                             ui_polybar->bar_config_page->height_combo_box);
        return;
    }

    check_int_values_with_combo_box(ui_polybar,
                                    ui_polybar->bar_config_page->height_spin_button,
                                    ui_polybar->bar_config_page->height_combo_box,
                                    "height");
}

void UIPolybarHelper::get_offset(Beans::UI::UIPolybar *ui_polybar)
{
    if (ui_polybar->key_exists_in_selected_bar("offset"))
    {
        check_int_values_with_combo_box(ui_polybar,
                                        ui_polybar->bar_config_page->offset_x_spin_button,
                                        ui_polybar->bar_config_page->offset_x_combo_box,
                                        "offset");

        check_int_values_with_combo_box(ui_polybar,
                                        ui_polybar->bar_config_page->offset_y_spin_button,
                                        ui_polybar->bar_config_page->offset_y_combo_box,
                                        "offset");
    }
    else if (ui_polybar->key_exists_in_selected_bar("offset-x"))
    {
        check_int_values_with_combo_box(ui_polybar,
                                        ui_polybar->bar_config_page->offset_x_spin_button,
                                        ui_polybar->bar_config_page->offset_x_combo_box,
                                        "offset-x");

        set_default_int_value_with_combo_box(ui_polybar,
                                             ui_polybar->bar_config_page->offset_y_spin_button,
                                             ui_polybar->bar_config_page->offset_y_combo_box);
    }
    else if (ui_polybar->key_exists_in_selected_bar("offset-y"))
    {
        check_int_values_with_combo_box(ui_polybar,
                                        ui_polybar->bar_config_page->offset_y_spin_button,
                                        ui_polybar->bar_config_page->offset_y_combo_box,
                                        "offset-y");

        set_default_int_value_with_combo_box(ui_polybar,
                                             ui_polybar->bar_config_page->offset_x_spin_button,
                                             ui_polybar->bar_config_page->offset_x_combo_box);
    }
    else
    {
        set_default_int_value_with_combo_box(ui_polybar,
                                             ui_polybar->bar_config_page->offset_x_spin_button,
                                             ui_polybar->bar_config_page->offset_x_combo_box);

        set_default_int_value_with_combo_box(ui_polybar,
                                             ui_polybar->bar_config_page->offset_y_spin_button,
                                             ui_polybar->bar_config_page->offset_y_combo_box);
    }
}

void UIPolybarHelper::get_colors(std::shared_ptr<Gtk::ComboBoxText> &combo_box)
{
    for (auto color : Beans::Manager::get_polybar_colors())
    {
        combo_box->append(color.first.str());
    }
    combo_box->append(NONE);
}

void UIPolybarHelper::get_background(Beans::UI::UIPolybar *ui_polybar)
{
    ui_polybar->bar_config_page->background_combo_box->remove_all();
    get_colors(ui_polybar->bar_config_page->background_combo_box);
    if (ui_polybar->key_exists_in_selected_bar("background"))
    {
        auto color_var = check_polybar_variable(ui_polybar->selected_bar->at("background"));
        ui_polybar->bar_config_page->background_combo_box->set_active_text(
            get_color_from_polybar_variable(color_var));
    }
}

void UIPolybarHelper::get_foreground(Beans::UI::UIPolybar *ui_polybar)
{
    ui_polybar->bar_config_page->foreground_combo_box->remove_all();
    get_colors(ui_polybar->bar_config_page->foreground_combo_box);
    if (ui_polybar->key_exists_in_selected_bar("foreground"))
    {
        auto color_var = check_polybar_variable(ui_polybar->selected_bar->at("foreground"));
        ui_polybar->bar_config_page->foreground_combo_box->set_active_text(
            get_color_from_polybar_variable(color_var));
    }
}

void UIPolybarHelper::get_radius(Beans::UI::UIPolybar *ui_polybar)
{
    if (ui_polybar->key_exists_in_selected_bar("radius"))
    {
        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->radius_top_spin_button,
                          0,
                          10,
                          0.1,
                          "radius");

        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->radius_bottom_spin_button,
                          0,
                          10,
                          0.1,
                          "radius");
    }
    else if (ui_polybar->key_exists_in_selected_bar("radius-top"))
    {
        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->radius_top_spin_button,
                          0,
                          10,
                          0.1,
                          "radius-top");

        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->radius_bottom_spin_button,
                                0,
                                10,
                                0.1);
    }
    else if (ui_polybar->key_exists_in_selected_bar("radius-bottom"))
    {
        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->radius_bottom_spin_button,
                          0,
                          10,
                          0.1,
                          "radius-bottom");

        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->radius_top_spin_button,
                                0,
                                10,
                                0.1);
    }
    else
    {
        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->radius_top_spin_button,
                                0,
                                10,
                                0.1);

        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->radius_bottom_spin_button,
                                0,
                                10,
                                0.1);
    }
}

void UIPolybarHelper::get_line_size(Beans::UI::UIPolybar *ui_polybar)
{
    if (ui_polybar->key_exists_in_selected_bar("line-size"))
    {
        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->overline_size_spin_button,
                          0,
                          4096,
                          1,
                          "line-size");

        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->underline_size_spin_button,
                          0,
                          4096,
                          1,
                          "line-size");
    }
    else if (ui_polybar->key_exists_in_selected_bar("overline-size"))
    {
        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->overline_size_spin_button,
                          0,
                          4096,
                          1,
                          "overline-size");

        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->underline_size_spin_button,
                                0,
                                4096,
                                1);
    }
    else if (ui_polybar->key_exists_in_selected_bar("underline-size"))
    {
        check_float_value(ui_polybar,
                          ui_polybar->bar_config_page->underline_size_spin_button,
                          0,
                          4096,
                          1,
                          "underline-size");

        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->overline_size_spin_button,
                                0,
                                4096,
                                1);
    }
    else
    {
        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->overline_size_spin_button,
                                0,
                                4096,
                                1);

        set_default_float_value(ui_polybar,
                                ui_polybar->bar_config_page->underline_size_spin_button,
                                0,
                                4096,
                                1);
    }
}

void UIPolybarHelper::get_monitor(Beans::UI::UIPolybar *ui_polybar)
{

    int number_of_monitors
        = Beans::Manager::get_window_obj()->get_window().get_screen()->get_n_monitors();

    ui_polybar->bar_config_page->monitor_combo_box->remove_all();
    for (int i = 0; i < number_of_monitors; i++)
    {
        ui_polybar->bar_config_page->monitor_combo_box->append(Beans::Manager::get_window_obj()
                                                                   ->get_window()
                                                                   .get_display()
                                                                   ->get_monitor(i)
                                                                   ->get_model());
    }

    if (ui_polybar->key_exists_in_selected_bar("monitor"))
    {
        ui_polybar->bar_config_page->monitor_combo_box->set_active_text(
            ui_polybar->selected_bar->at("monitor").str());
    }
}

void UIPolybarHelper::get_monitor_fallback(Beans::UI::UIPolybar *ui_polybar)
{
    int number_of_monitors
        = Beans::Manager::get_window_obj()->get_window().get_screen()->get_n_monitors();

    ui_polybar->bar_config_page->monitor_fallback_combo_box->remove_all();
    for (int i = 0; i < number_of_monitors; i++)
    {
        ui_polybar->bar_config_page->monitor_fallback_combo_box->append(
            Beans::Manager::get_window_obj()
                ->get_window()
                .get_display()
                ->get_monitor(i)
                ->get_model());
    }
    ui_polybar->bar_config_page->monitor_fallback_combo_box->append(NONE);

    if (ui_polybar->key_exists_in_selected_bar("monitor-fallback"))
    {
        ui_polybar->bar_config_page->monitor_fallback_combo_box->set_active_text(
            ui_polybar->selected_bar->at("monitor-fallback").str());
    }
}

void UIPolybarHelper::assert_bar_config(Beans::UI::UIPolybar *ui_polybar)
{
    get_monitor(ui_polybar);
    get_monitor_fallback(ui_polybar);
    get_monitor_strict(ui_polybar);
    get_monitor_exact(ui_polybar);
    get_override_redirect(ui_polybar);
    get_bottom(ui_polybar);
    get_fixed_center(ui_polybar);
    get_width(ui_polybar);
    get_height(ui_polybar);
    get_offset(ui_polybar);
    get_background(ui_polybar);
    get_foreground(ui_polybar);
    get_radius(ui_polybar);
    get_line_size(ui_polybar);
}

void UIPolybarHelper::save_ui_combo_box(Beans::UI::UIPolybar *                    ui_polybar,
                                        HashMap<std::string, Utils::WideString> * p,
                                        std::string                               name,
                                        const std::shared_ptr<Gtk::ComboBoxText> &combo_box)
{
    Utils::WideString value = combo_box->get_active_text().raw();

    if (ui_polybar->key_exists_in_selected_bar(name))
    {
        if (value != NONE)
            p->at(name) = combo_box->get_active_text().raw();
        else
            p->erase(name);
    }
    else
    {
        if (value != NONE)
            p->insert(
                std::pair<std::string, Utils::WideString>(name, combo_box->get_active_text().raw()));
    }
}

void UIPolybarHelper::save_ui_color_combo_box(Beans::UI::UIPolybar *                    ui_polybar,
                                              HashMap<std::string, Utils::WideString> * p,
                                              std::string                               name,
                                              const std::shared_ptr<Gtk::ComboBoxText> &combo_box)
{
    Utils::WideString value = combo_box->get_active_text().raw();
    if (ui_polybar->key_exists_in_selected_bar(name))
    {
        if (value != NONE)
            p->at(name) = "${colors." + combo_box->get_active_text().raw() + "}";
        else
            p->erase(name);
    }
    else
    {
        if (value != NONE)
            p->insert(std::pair<std::string, Utils::WideString>(
                name,
                "${colors." + combo_box->get_active_text().raw() + "}"));
    }
}

void UIPolybarHelper::save_ui_spin_button(Beans::UI::UIPolybar *                   ui_polybar,
                                          HashMap<std::string, Utils::WideString> *p,
                                          std::string                              name,
                                          const std::shared_ptr<Gtk::SpinButton> & spin_button)
{
    if (ui_polybar->key_exists_in_selected_bar(name))
    {
        p->at(name) = Utils::double_to_string(spin_button->get_value());
    }
    else
    {
        p->insert(std::pair<std::string, Utils::WideString>(name, Utils::double_to_string(spin_button->get_value())));
    }
}

void UIPolybarHelper::save_ui_spin_button_with_combo_box(
    Beans::UI::UIPolybar *                    ui_polybar,
    HashMap<std::string, Utils::WideString> * p,
    std::string                               name,
    const std::shared_ptr<Gtk::ComboBoxText> &combo_box,
    const std::shared_ptr<Gtk::SpinButton> &  spin_button)

{
    if (ui_polybar->key_exists_in_selected_bar(name))
    {
        p->at(name) = Utils::double_to_string(spin_button->get_value())
                      + (combo_box->get_active_text() == "%" ? "%" : "");
    }
    else
    {
        p->insert(std::pair<std::string, Utils::WideString>(
            name,
            Utils::double_to_string(spin_button->get_value())
                + (combo_box->get_active_text() == "%" ? "%" : "")));
    }
}

void UIPolybarHelper::save_ui_check_button(Beans::UI::UIPolybar *                   ui_polybar,
                                           HashMap<std::string, Utils::WideString> *p,
                                           std::string                              name,
                                           const std::shared_ptr<Gtk::CheckButton> &check_button)
{
    if (ui_polybar->key_exists_in_selected_bar(name))
    {
        p->at(name) = check_button->get_active() ? "true" : "false";
    }
    else
    {
        p->insert(std::pair<std::string, Utils::WideString>(name,
                                                            check_button->get_active() ? "true" :
                                                                                         "false"));
    }
}

void UIPolybarHelper::save_bar_config(Beans::UI::UIPolybar *ui_polybar)
{
    if (ui_polybar->selected_bar == nullptr)
    {
        return;
    }

    const auto p = ui_polybar->selected_bar.get();

    save_ui_combo_box(ui_polybar, p, "monitor", ui_polybar->bar_config_page->monitor_combo_box);
    save_ui_combo_box(ui_polybar, p, "monitor-fallback", ui_polybar->bar_config_page->monitor_fallback_combo_box);
    save_ui_check_button(ui_polybar, p, "monitor-strict", ui_polybar->bar_config_page->monitor_strict_check_button);
    save_ui_check_button(ui_polybar, p, "monitor-exact", ui_polybar->bar_config_page->monitor_exact_check_button);
    save_ui_check_button(ui_polybar, p, "override-redirect", ui_polybar->bar_config_page->override_redirect_check_button);
    save_ui_check_button(ui_polybar, p, "bottom", ui_polybar->bar_config_page->bottom_check_button);
    save_ui_check_button(ui_polybar, p, "fixed-center", ui_polybar->bar_config_page->fixed_center_check_button);
    
    save_ui_spin_button_with_combo_box(ui_polybar,
                                       p,
                                       "width",
                                       ui_polybar->bar_config_page->width_combo_box,
                                       ui_polybar->bar_config_page->width_spin_button);

    save_ui_spin_button_with_combo_box(ui_polybar,
                                       p,
                                       "height",
                                       ui_polybar->bar_config_page->height_combo_box,
                                       ui_polybar->bar_config_page->height_spin_button);

    save_ui_spin_button_with_combo_box(ui_polybar,
                                       p,
                                       "offset-x",
                                       ui_polybar->bar_config_page->offset_x_combo_box,
                                       ui_polybar->bar_config_page->offset_x_spin_button);

    save_ui_spin_button_with_combo_box(ui_polybar,
                                       p,
                                       "offset-y",
                                       ui_polybar->bar_config_page->offset_y_combo_box,
                                       ui_polybar->bar_config_page->offset_y_spin_button);

    save_ui_color_combo_box(ui_polybar,
                            p,
                            "background",
                            ui_polybar->bar_config_page->background_combo_box);

    save_ui_color_combo_box(ui_polybar,
                            p,
                            "foreground",
                            ui_polybar->bar_config_page->foreground_combo_box);

    // TODO: Resolve conflict between "radius" and "radius-top"
    save_ui_spin_button(ui_polybar,
                        p,
                        "radius-top",
                        ui_polybar->bar_config_page->radius_top_spin_button);
}

} // namespace UI
} // namespace Beans
