#include "ui.hpp"

namespace Beans
{
namespace UI
{

void UITemplate::show()
{
    this->window->show_all();
}

int DialogTemplate::show()
{
    return this->dialog->run();
}

} // namespace UI
} // namespace Beans