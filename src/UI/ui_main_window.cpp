#include "ui_main_window.hpp"

#include <iostream>

namespace Beans
{
namespace UI
{

UIMain::UIMain()
{
    this->xml_file_path = std::filesystem::current_path().parent_path().append("UI/config.glade");
    this->builder
        = Gtk::Builder::create_from_file(std::filesystem::absolute(this->xml_file_path).native());

    this->window        = this->get_widget_from_builder<Gtk::Window>("main_window");
    this->stack_sidebar = this->get_widget_from_builder<Gtk::StackSidebar>("stack_sidebar");
    this->ui_polybar    = std::make_shared<UI::UIPolybar>();

    this->initialize_polybar_page();
}

void UIMain::initialize_polybar_page()
{
    this->ui_polybar->color_config_window = std::make_unique<UIColorConfig>(
        this->get_widget_from_builder<Gtk::Button>("color_config_cancel_button"),
        this->get_widget_from_builder<Gtk::Button>("color_config_apply_button"),
        this->get_widget_from_builder<Gtk::ColorButton>("color_config_color_button"),
        this->get_widget_from_builder<Gtk::Entry>("color_config_color_name_entry"),
        this->get_widget_from_builder<Gtk::SpinButton>("color_config_spin_button"),
        this->get_widget_from_builder<Gtk::Window>("color_config_window"));

    this->ui_polybar->color_page_init(
        this->get_widget_from_builder<Gtk::Revealer>("polybar_color_notification_revealer"),
        this->get_widget_from_builder<Gtk::Spinner>("polybar_color_notification_spinner"),
        this->get_widget_from_builder<Gtk::Label>("polybar_color_notification_label"),
        this->get_widget_from_builder<Gtk::TreeView>("polybar_color_tree_view"),
        this->get_widget_from_builder<Gtk::Button>("polybar_color_save_button"),
        this->get_widget_from_builder<Gtk::Button>("polybar_color_delete_button"),
        this->get_widget_from_builder<Gtk::Button>("polybar_color_edit_button"),
        this->get_widget_from_builder<Gtk::Button>("polybar_color_new_button"),
        this->builder->get_object("polybar_color_list_model"));

    this->ui_polybar->bar_selection_page_init(
        this->get_widget_from_builder<Gtk::Revealer>("polybar_bar_selection_notification_revealer"),
        this->get_widget_from_builder<Gtk::Spinner>("polybar_bar_selection_notification_spinner"),
        this->get_widget_from_builder<Gtk::Label>("polybar_bar_selection_notification_label"),
        this->get_widget_from_builder<Gtk::TreeView>("polybar_bar_selection_tree_view"),
        this->get_widget_from_builder<Gtk::Button>("polybar_bar_selection_save_button"),
        this->get_widget_from_builder<Gtk::Button>("polybar_bar_selection_delete_button"),
        this->get_widget_from_builder<Gtk::Button>("polybar_bar_selection_edit_button"),
        this->get_widget_from_builder<Gtk::Button>("polybar_bar_selection_new_button"),
        this->builder->get_object("polybar_bar_list_model"));

    this->ui_polybar->bar_config_page_init(
        this->get_widget_from_builder<Gtk::Label>("polybar_bar_name_label"),
        this->get_widget_from_builder<Gtk::Revealer>("polybar_bar_config_notification_revealer"),
        this->get_widget_from_builder<Gtk::Spinner>("polybar_bar_config_notification_spinner"),
        this->get_widget_from_builder<Gtk::Label>("polybar_bar_config_notification_label"),
        this->get_widget_from_builder<Gtk::Button>("polybar_bar_config_back_button"),
        this->get_widget_from_builder<Gtk::Button>("polybar_bar_config_save_button"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_monitor_combo_box"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_monitor_fallback_combo_box"),
        this->get_widget_from_builder<Gtk::CheckButton>("polybar_monitor_strict_check_button"),
        this->get_widget_from_builder<Gtk::CheckButton>("polybar_monitor_exact_check_button"),
        this->get_widget_from_builder<Gtk::CheckButton>("polybar_override_redirect_check_button"),
        this->get_widget_from_builder<Gtk::CheckButton>("polybar_bottom_check_button"),
        this->get_widget_from_builder<Gtk::CheckButton>("polybar_fixed_center_check_button"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_width_spin_button"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_width_combo_box"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_height_spin_button"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_height_combo_box"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_offset_x_spin_button"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_offset_x_combo_box"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_offset_y_spin_button"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_offset_y_combo_box"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_background_combo_box"),
        this->get_widget_from_builder<Gtk::Button>("polybar_background_gradient_edit_button"),
        this->get_widget_from_builder<Gtk::ComboBoxText>("polybar_foreground_combo_box"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_radius_top_spin_button"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_radius_bottom_spin_button"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_overline_size_spin_button"),
        this->get_widget_from_builder<Gtk::SpinButton>("polybar_underline_size_spin_button"));

    this->ui_polybar->delete_dialog = std::make_unique<DialogDelete>(
        this->get_widget_from_builder<Gtk::Button>("delete_dialog_no_button"),
        this->get_widget_from_builder<Gtk::Button>("delete_dialog_yes_button"),
        this->get_widget_from_builder<Gtk::Label>("delete_dialog_label"),
        this->get_widget_from_builder<Gtk::Dialog>("delete_dialog"));

    this->ui_polybar->list_selection_window = std::make_unique<UIListSelection>(
        this->get_widget_from_builder<Gtk::TreeView>("list_selection_tree_view"),
        this->get_widget_from_builder<Gtk::Button>("list_selection_up_button"),
        this->get_widget_from_builder<Gtk::Button>("list_selection_down_button"),
        this->get_widget_from_builder<Gtk::Button>("list_selection_cancel_button"),
        this->get_widget_from_builder<Gtk::Button>("list_selection_apply_button"),
        this->get_widget_from_builder<Gtk::Window>("list_selection_window"));

    this->ui_polybar->stack = this->get_widget_from_builder<Gtk::Stack>("polybar_config_stack");
    this->ui_polybar->bars_substack
        = this->get_widget_from_builder<Gtk::Stack>("polybar_bars_substack");

    this->ui_polybar->color_page_list();
    this->ui_polybar->bar_selection_page_list();
}

} // namespace UI
} // namespace Beans
