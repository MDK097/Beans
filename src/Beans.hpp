#pragma once
#include "Parsers/polybar_parser.hpp"
#include "UI/ui_main_window.hpp"
#include "UI/ui_polybar.hpp"
#include "Utils/Logger/logger.hpp"
#include "Utils/utils.hpp"
#include "Utils/wide_string.hpp"

#include <codecvt>
#include <fstream>
#include <gtkmm-3.0/gtkmm.h>
#include <locale>
#include <memory>
#include <string>
#include <unordered_map>

#define HashMap std::unordered_map

using File = std::fstream;
using Log::LOG_LEVEL;
using Log::LogLevel;
using polybar_parser::PolybarParser;
using std::forward_list;
using std::string;
using Utils::to_string;
using Utils::WideString;

namespace Beans
{

class Manager
{
    private:
    /* Attributes */
    static string POLYBAR_CONFIG_FILE_PATH;

    static Glib::RefPtr<Gtk::Application>  app;
    static PolybarParser                   p_parser;
    static std::shared_ptr<UI::UITemplate> window_obj;

    public:
    /* Methods */
    static void init(int &argc, char **&argv);
    static int  run();
    static void save();

    static const HashMap<WideString, string> &                               get_polybar_colors();
    static const forward_list<std::shared_ptr<HashMap<string, WideString>>> &get_polybar_bars();
    static void                            set_polybar_color_value(WideString key, string value);
    static void                            remove_polybar_color_value(WideString key);
    static void                            remove_polybar_bar(WideString name);
    static void                            refresh_ui_polybar_color_list();
    static void                            refresh_ui_polybar_bar_list();
    static void                            run_polybar_parser();
    static std::shared_ptr<UI::UITemplate> get_window_obj();
};

} // namespace Beans
