#include "Beans.hpp"

namespace Beans
{

Glib::RefPtr<Gtk::Application>  Manager::app;
std::shared_ptr<UI::UITemplate> Manager::window_obj;
PolybarParser                   Manager::p_parser;

string Manager::POLYBAR_CONFIG_FILE_PATH
    = string("/home/") + Utils::get_user() + "/.config/polybar/config";

void Manager::init(int &argc, char **&argv)
{
    app = Gtk::Application::create(argc, argv, "com.github.mdk97.Beans");
    std::shared_ptr<UI::UIMain> temp = std::make_shared<UI::UIMain>();
    window_obj = std::static_pointer_cast<UI::UITemplate, UI::UIMain>(temp);
}

int Manager::run()
{
    return app->run(window_obj->get_window());
}

void Manager::save()
{
    p_parser.WriteFile(POLYBAR_CONFIG_FILE_PATH);
}

const HashMap<WideString, string> &Manager::get_polybar_colors()
{
    return p_parser.get_colors();
}

void Manager::refresh_ui_polybar_color_list()
{
    std::dynamic_pointer_cast<UI::UIMain, UI::UITemplate>(window_obj)
        ->ui_polybar->color_page_list();
}

void Manager::refresh_ui_polybar_bar_list()
{
    std::dynamic_pointer_cast<UI::UIMain, UI::UITemplate>(window_obj)
        ->ui_polybar->bar_selection_page_list();
}

void Manager::set_polybar_color_value(WideString key, string value)
{
    p_parser.set_color_value(key, value);
}

void Manager::remove_polybar_color_value(WideString key)
{
    p_parser.remove_color_value(key);
}

const forward_list<std::shared_ptr<HashMap<string, WideString>>> &Manager::get_polybar_bars()
{
    return p_parser.get_bars();
}

void Manager::remove_polybar_bar(WideString name)
{
    p_parser.remove_bar(name);
}

void Manager::run_polybar_parser()
{
    p_parser.ParseFile(POLYBAR_CONFIG_FILE_PATH);
}

std::shared_ptr<UI::UITemplate> Manager::get_window_obj()
{
    return (window_obj);
}

} // namespace Beans
