#include "Beans.hpp"

int main(int argc, char *argv[])
{
    Beans::Manager::init(argc, argv);
    Beans::Manager::run();
    return 0;
}