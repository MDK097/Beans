#pragma once

#include "../Utils/Logger/logger.hpp"
#include "../Utils/wide_string.hpp"

#include <codecvt>
#include <filesystem>
#include <forward_list>
#include <fstream>
#include <locale>
#include <memory>
#include <regex>
#include <string>
#include <unordered_map>

#define HashMap std::unordered_map

namespace polybar_parser
{

using std::forward_list;
using std::string;
using Utils::WideString;
using File = std::fstream;
using std::regex;
using sstream = std::stringstream;
using std::shared_ptr;
using std::smatch;

WideString ReadLineFromFile(File &file_handler);

class PolybarParser
{
    enum SectionType
    {
        NONE,
        COLOR,
        BAR,
        MODULE
    };

    public:
    PolybarParser();
    void ParseFile(const std::filesystem::path file_path);
    void WriteFile(const std::filesystem::path file_path);

    inline const HashMap<WideString, string> &get_colors() { return this->colors; }
    inline const forward_list<shared_ptr<HashMap<string, WideString>>> &get_bars()
    {
        return this->bars;
    }

    inline const forward_list<shared_ptr<HashMap<string, WideString>>> &get_modules()
    {
        return this->modules;
    }

    inline void set_color_value(WideString key, string value) { this->colors[key.w_str()] = value; }
    inline void remove_color_value(WideString key)
    {
        this->colors.erase(this->colors.find(key.w_str()));
    }
    void remove_bar(WideString name);

    private:
    HashMap<WideString, string>                           colors;
    forward_list<shared_ptr<HashMap<string, WideString>>> bars;
    forward_list<shared_ptr<HashMap<string, WideString>>> modules;
};

} // namespace polybar_parser
