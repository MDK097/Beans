#include "polybar_parser.hpp"

#include "../Utils/utils.hpp"

#include <iostream>

namespace polybar_parser
{

using std::cout;
using std::endl;
using Utils::to_string;

const string CONFIG_FILE_PATH = "";

PolybarParser::PolybarParser()
{
}

WideString ReadLineFromFile(File &file_handle)
{
    WideString s("");
    s.get_line_from(std::getline, file_handle);
    return (s);
}

void PolybarParser::ParseFile(const std::filesystem::path file_path)
{
    File config_file;

    Log::Show(Log::LogLevel::DEBUG, "Trying to open config file. . .");
    config_file.open(file_path, File::in);

    if (not config_file.is_open())
    {
        Log::Show(Log::LogLevel::ERROR, "Error opening file!\n");
        if (not std::filesystem::exists(file_path))
        {
            Log::Indent(Log::LogLevel::WARNING, 1, " ==> ");
            Log::Show(Log::LogLevel::WARNING, CONFIG_FILE_PATH + " does not exist!\n");
        }
        exit(-1);
    }

    Log::Show(Log::LogLevel::DEBUG, "Done!\n");

    shared_ptr<HashMap<string, WideString>> current_bar, current_module;
    WideString                              bar_name;
    WideString                              module_name;
    WideString                              line;

    smatch matches;

    regex color_reg("^\\[colors\\]");
    regex bar_reg("^\\[bar\\/((?:\\w+|[-]+)+)\\]$");
    regex module_reg("^\\[module\\/((?:\\w+.*)+)\\]$");

    regex color_value_reg("((?:\\w+|[-]+)+)[\t ]*=[\t ]*([#](?:\\w{8}|\\w{6}|\\w{4}|\\w{3}))");
    regex bar_value_reg("((?:\\w+|[-]+)+)[\t ]*=[\t ]*(.+)");
    regex module_value_reg("((?:\\w+|[-]+)+)[\t ]*=[\t ]*(.+)");

    uint        current_line = 0;
    SectionType id           = NONE;

    Log::Show(Log::LogLevel::DEBUG, "Allocating memory for smart pointers. . .");
    current_bar    = std::make_shared<HashMap<string, WideString>>();
    current_module = std::make_shared<HashMap<string, WideString>>();

    Log::Show(Log::LogLevel::DEBUG, "Done!\n");

    while (true)
    {
        current_line++;

        string temp;

        Log::Show(Log::LogLevel::DEBUG, "Reading line from file. . .");
        line = ReadLineFromFile(config_file);
        temp = line.str();

        if (config_file.eof())
        {
            Log::Show(Log::LogLevel::DEBUG, "EOF\n");

            Log::Show(Log::LogLevel::DEBUG, "Pushing current_module into internal vector");
            Log::Show(Log::LogLevel::INFO,
                      string(": Address [") + to_string(current_bar.get()) + "]");
            Log::Show(Log::LogLevel::DEBUG, ". . .");
            this->modules.push_front(current_module);
            current_module = std::make_shared<HashMap<string, WideString>>();
            Log::Show(Log::LogLevel::DEBUG, "Done!\n");
            break;
        }
        else if (config_file.fail())
        {
            Log::Show(Log::LogLevel::ERROR, "Error while reading from file!\n");
            exit(-1);
        }

        Log::Show(Log::LogLevel::DEBUG, "Done!");

        Log::Indent(Log::LogLevel::INFO, 0, " -> ");
        Log::Show(Log::LogLevel::INFO, temp);
        Log::NewLine(Log::LogLevel::DEBUG);

        temp = line.str();
        if (regex_search(temp, matches, color_reg))
        {
            id = COLOR;
            Log::Indent(Log::LogLevel::INFO, 1, "==> ");
            Log::Show(Log::LogLevel::INFO, "id = COLOR\n");
        }
        else if (regex_search(temp, matches, bar_reg))
        {
            Log::Indent(Log::LogLevel::INFO, 1, "==> ");
            Log::Show(Log::LogLevel::INFO,
                      string("Regex match successful: ") + matches.str(1) + "\n");
            Log::Show(Log::LogLevel::WARNING, "New bar definition found!\n");
            bar_name = matches.str(1);

            if (id == BAR)
            {
                Log::Show(Log::LogLevel::DEBUG, "Pushing current_bar into internal vector");
                Log::Show(Log::LogLevel::INFO,
                          string(": Address [") + to_string(current_bar.get()) + "]");
                Log::Show(Log::LogLevel::DEBUG, ". . .");
                this->bars.push_front(current_bar);
                current_bar = std::make_shared<HashMap<string, WideString>>();
                Log::Show(Log::LogLevel::DEBUG, "Done!\n");
            }

            Log::Indent(Log::LogLevel::INFO, 1, "==> ");
            Log::Show(Log::LogLevel::INFO, string("bar_name = ") + bar_name.str() + "\n");
            Log::Show(Log::LogLevel::DEBUG, "Inserting new name into current_bar. . .");
            current_bar->insert(std::pair<string, WideString>("name", bar_name));
            Log::Show(Log::LogLevel::DEBUG, "Done!\n");

            id = BAR;
            Log::Indent(Log::LogLevel::INFO, 1, "==> ");
            Log::Show(Log::LogLevel::INFO, "id = BAR\n");
        }
        else if (regex_search(temp, matches, module_reg))
        {
            Log::Indent(Log::LogLevel::INFO, 1, "==> ");
            Log::Show(Log::LogLevel::INFO,
                      string("Regex match successful: ") + matches.str(1) + "\n");
            Log::Show(Log::LogLevel::WARNING, "New module definition found!\n");
            module_name = matches.str(1);

            if (id == BAR)
            {
                Log::Show(Log::LogLevel::DEBUG, "Pushing current_bar into internal vector");
                Log::Show(Log::LogLevel::INFO,
                          string(": Address [") + to_string(current_bar.get()) + "]");
                Log::Show(Log::LogLevel::DEBUG, ". . .");
                this->bars.push_front(current_bar);
                current_bar = std::make_shared<HashMap<string, WideString>>();
                Log::Show(Log::LogLevel::DEBUG, "Done!\n");
            }
            else if (id == MODULE)
            {
                Log::Show(Log::LogLevel::DEBUG, "Pushing current_module into internal vector");
                Log::Show(Log::LogLevel::INFO,
                          string(": Address [") + to_string(current_bar.get()) + "]");
                Log::Show(Log::LogLevel::DEBUG, ". . .");
                this->modules.push_front(current_module);
                current_module = std::make_shared<HashMap<string, WideString>>();
                Log::Show(Log::LogLevel::DEBUG, "Done!\n");
            }

            Log::Indent(Log::LogLevel::INFO, 1, "==> ");
            Log::Show(Log::LogLevel::INFO, string("module_name = ") + module_name.str() + "\n");
            Log::Show(Log::LogLevel::DEBUG, "Inserting new name into current_module. . .");
            current_module->insert(std::pair<string, WideString>("name", module_name));
            Log::Show(Log::LogLevel::DEBUG, "Done!\n");

            id = MODULE;
            Log::Indent(Log::LogLevel::INFO, 1, "==> ");
            Log::Show(Log::LogLevel::INFO, "id = MODULE\n");
        }
        else
        {
            if (id == NONE)
            {
                string error_message
                    = "Invalid syntax at line " + to_string(current_line) + ": " + temp + "\n";
                Log::Show(Log::LogLevel::ERROR, error_message);
                exit(-1);
            }
            else if (id == COLOR)
            {
                if (regex_search(temp, matches, color_value_reg))
                {
                    Log::Show(Log::LogLevel::WARNING, "New color value found!\n");
                    Log::Indent(Log::LogLevel::INFO, 1, "==> ");
                    Log::Show(Log::LogLevel::INFO,
                              string("(") + matches.str(1) + ", " + matches.str(2) + ")\n");
                    Log::Show(Log::LogLevel::DEBUG, "Inserting new value into colors. . .");

                    WideString temp(matches.str(1).c_str());
                    this->colors.insert(std::pair<WideString, string>(temp, matches.str(2)));

                    Log::Show(Log::LogLevel::DEBUG, "Done!\n");
                }
                else if (temp.empty())
                {
                    continue;
                }
                else
                {
                    string error_message
                        = "Invalid syntax at line " + to_string(current_line) + ": " + temp + "\n";
                    Log::Show(Log::LogLevel::ERROR, error_message);
                    exit(-1);
                }
            }
            else if (id == BAR)
            {
                if (regex_search(temp, matches, bar_value_reg))
                {
                    Log::Show(Log::LogLevel::WARNING, "New bar value found!\n");
                    Log::Indent(Log::LogLevel::INFO, 1, "==> ");
                    Log::Show(Log::LogLevel::INFO,
                              string("(") + matches.str(1) + ", " + matches.str(2) + ")\n");
                    Log::Show(Log::LogLevel::DEBUG, "Inserting new value into current_bar. . .");

                    WideString temp(matches.str(2).c_str());
                    current_bar->insert(std::pair<string, WideString>(matches.str(1), temp));

                    Log::Show(Log::LogLevel::DEBUG, "Done!\n");
                }
                else if (temp.empty())
                {
                    continue;
                }
                else
                {
                    string error_message
                        = "Invalid syntax at line " + to_string(current_line) + ": " + temp + "\n";
                    Log::Show(Log::LogLevel::ERROR, error_message);
                    exit(-1);
                }
            }
            else if (id == MODULE)
            {
                if (regex_search(temp, matches, module_value_reg))
                {
                    Log::Show(Log::LogLevel::WARNING, "New module value found!\n");
                    Log::Indent(Log::LogLevel::INFO, 1, "==> ");
                    Log::Show(Log::LogLevel::INFO,
                              string("(") + matches.str(1) + ", " + matches.str(2) + ")\n");
                    Log::Show(Log::LogLevel::DEBUG, "Inserting new value into current_module. . .");

                    WideString temp(matches.str(2).c_str());
                    current_module->insert(std::pair<string, WideString>(matches.str(1), temp));

                    Log::Show(Log::LogLevel::DEBUG, "Done!\n");
                }
                else if (temp.empty())
                {
                    continue;
                }
                else
                {
                    string error_message
                        = "Invalid syntax at line " + to_string(current_line) + ": " + temp + "\n";
                    Log::Show(Log::LogLevel::ERROR, error_message);
                    exit(-1);
                }
            }
        }
    }
}

void PolybarParser::WriteFile(const std::filesystem::path file_path)
{
    WideString wide_string;
    auto       backup = file_path;
    backup += ".backup";
    if (std::filesystem::exists(std::filesystem::absolute(backup).native()))
        std::filesystem::remove(std::filesystem::absolute(backup).native());
    std::filesystem::copy_file(file_path, std::filesystem::absolute(backup).native());

    File config_file;
    config_file.open(file_path, File::out);

    if (not config_file.is_open())
    {
        Log::Show(Log::LogLevel::ERROR, "Error opening file!\n");
        if (not std::filesystem::exists(file_path))
        {
            Log::Indent(Log::LogLevel::WARNING, 1, " ==> ");
            Log::Show(Log::LogLevel::WARNING, CONFIG_FILE_PATH + " does not exist!\n");
        }
        exit(-1);
    }

    config_file << "[colors]\n";
    for (auto color : this->colors)
    {
        wide_string = color.first;
        config_file << wide_string << " = " << color.second << "\n";
    }

    for (auto bar : this->bars)
    {
        wide_string = bar->at("name");
        config_file << "\n[bar/" << wide_string << "]\n";
        for (auto entry : *bar)
        {
            if (entry.first != "name")
            {
                wide_string = entry.second;
                config_file << entry.first << " = " << wide_string << "\n";
            }
        }
    }

    for (auto module : this->modules)
    {
        wide_string = module->at("name");
        config_file << "\n[module/" << wide_string << "]\n";
        for (auto entry : *module)
        {
            if (entry.first != "name")
            {
                wide_string = entry.second;
                config_file << entry.first << " = " << wide_string << "\n";
            }
        }
    }
}

void PolybarParser::remove_bar(WideString name)
{
    std::shared_ptr<HashMap<string, WideString>> temp;
    for (auto bar : this->bars)
    {
        WideString w(bar->at("name"));
        if (w == name)
        {
            temp = bar;
        }
    }
    this->bars.remove(temp);
}

} // namespace polybar_parser
