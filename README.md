# Beans
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg?style=for-the-badge)](https://www.gnu.org/licenses/gpl-3.0)
![GitHub top language](https://img.shields.io/github/languages/top/mdk97/Beans?style=for-the-badge)
![GitHub commit activity](https://img.shields.io/github/commit-activity/m/mdk97/Beans?style=for-the-badge)
![GitHub last commit (branch)](https://img.shields.io/github/last-commit/mdk97/Beans/master?style=for-the-badge)

## Goes well with your [rice](https://www.reddit.com/r/unixporn/wiki/themeing/dictionary#wiki_rice)! :rice:

Beans is a WIP GUI utility to help you configure [i3](https://i3wm.org/), [Polybar](https://github.com/polybar/polybar) and hopefully many other [ricing](https://www.reddit.com/r/unixporn/wiki/themeing/dictionary#wiki_rice) related software that rely on text files for configuration.<br>

<img src="misc/Beans.gif" width=800 height=600 />

## Some important observations
I consider this implementation to be just a proof of concept for now.<br>

## Roadmap - What has been done so far
 1. Beans was created from scratch, using a simple perl script to parse the configuration files and it had a simple CLI interface to interact with it.
 2. Then, I began improving the interface alogside the functionality, making menus for the user to choose the values from. The options are given by the [Polybar configuration wiki](https://github.com/polybar/polybar/wiki/Configuration). I started using [XDialog](http://xdialog.free.fr/#SHOTS) as its main interface, but various text formatting bugs appeared.
 3. I decided to migrate to the ncurses based terminal [Dialog](https://en.wikipedia.org/wiki/Dialog_(software)). At this point everything was fine as far as the interface was concerned. But Beans was becoming harder and harder to maintain as its codebase was growing ever faster. At some point, it had around 1500 lines of code on a single perl script and it wasn't even half done (I'm talking just about the polybar config parser). It was a mess, and something had to change.
 4. I splitted the functionalities of Beans into modules. It was supposed to have a backend that served the purpose of parsing the config files and generate JSON files based on them. I soon gave up on this idea.
 5. I started GUI development with GTK using C++. As I wasn't sure how to integrate the Perl parser into my C++ code, I decided to port my parser to C++ and I ditched the Perl scripts for good. Now everything seems to be well integrated and I hope to be on the right track.
 6. The GTK UI suffered severe changes from the original design and now it looks much better.
 
 ![](https://en.meming.world/images/en/thumb/2/26/Oh_Yeah%2C_It's_All_Coming_Together.jpg/300px-Oh_Yeah%2C_It's_All_Coming_Together.jpg)

## Build Dependencies
- [gtkmm](https://www.gtkmm.org/en/) 3.24.2
- [meson](https://mesonbuild.com/) >= 0.54.3
- [ninja](https://github.com/ninja-build/ninja) >= 1.10.0
- [gcc](https://gcc.gnu.org/) >= 10.1.0
- [GNU make](https://www.gnu.org/software/make/) >= 4.3
<br>
